﻿using GymManagment.Core.ViewModels;
using GymManagment.Shared.ViewModels;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace GymManagment.Repository.Interfaces
{
	public interface IMediaRepository
	{
		Task<string> Save(string userId, UploadFilePostModel model);

		Task<ImagesSaveViewModel> SaveMedia(string userId, IFormFile file, string folderName = "");
	}
}
