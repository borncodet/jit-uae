﻿using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;

namespace GymManagment.Repository.Interfaces
{
    public interface IDataService
    {
        IDbConnection GetDbConnection();

        Task<TEntity> GetAsync<TEntity>(string query);
        Task<TEntity> GetAsync<TEntity>(string query, IDbTransaction transaction = null);
        Task<TEntity> GetAsync<TEntity>(string query, object param);

        Task<IEnumerable<TEntity>> GetEnumerableAsync<TEntity>(string query);
        Task<IEnumerable<TEntity>> GetEnumerableAsync<TEntity>(string query, object Param);

        IEnumerable<TEntity> GetEnumerable<TEntity>(string query);
        IEnumerable<TEntity> GetEnumerable<TEntity>(string query, object param);

        Task<int> InsertOrUpdateAsync(string query);
        Task<int> InsertOrUpdateAsync<TEntity>(string query, TEntity model, IDbTransaction transaction = null);

        Task<string> ExecuteScalarAsync<TEntity>(string query, TEntity model, IDbTransaction transaction = null);
        Task<string> ExecuteScalarAsync(string query, IDbTransaction transaction = null);
        int ExecuteScalar(string query);

        Task<int> ExecuteAsync<TEntity>(string query, TEntity model, IDbTransaction transaction = null);
        //Task<PagedList<T>> GetPagedList<T>(string query, int pageIndex = 0, int pageSize = int.MaxValue);

        Task<int> DeleteAsync(string query, object param);

    }

    public interface IDataService2 : IDataService
    {

    }
}
