﻿using GymManagment.Core.ViewModels;
using GymManagment.Repository.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GymManagment.Repository
{

    public interface ICMSRepository
    {
        Task<IEnumerable<CMSContentViewModel>> GetCMSContent(string keys, int rowCount = 0);
        Task<IEnumerable<CMSContentViewModel>> GetCMSContent(IEnumerable<string> keys);
        Task<BlogViewModel> GetBlogByUrl(string url);
        Task<IEnumerable<BlogViewModel>> GetBlogs(int rowCount = 0);
        Task<IEnumerable<NewsViewModel>> GetNews(int rowCount = 0);
    }

    public class CMSRepository : ICMSRepository
    {
        private readonly IDataService _dataService;

        public CMSRepository(IDataService dataService)
        {
            _dataService = dataService;
        }

        public async Task<IEnumerable<CMSContentViewModel>> GetCMSContent(string keys, int rowCount = 0)
        {
            string strCount = "";
            if (rowCount > 0)
            {
                strCount = $" Top({rowCount}) ";
            }

            var res = await _dataService.GetEnumerableAsync<CMSContentViewModel>($@"SELECT {strCount} S.*,M.FileName
            FROM  CMSContents S
            JOIN Medias M on M.Id=S.MediaId
            Where S.IsActive=1 and S.CMSKey ='{keys}'");
            return res;
        }

        public async Task<IEnumerable<CMSContentViewModel>> GetCMSContent(IEnumerable<string> keys)
        {
            var res = await _dataService.GetEnumerableAsync<CMSContentViewModel>($@"SELECT  S.*,M.FileName
            FROM  CMSContent S
            JOIN Medias M on M.Id=S.MediaId
            Where S.IsActive=1 and S.CMSKey in('{keys}')");
            return res;
        }

        public async Task<BlogViewModel> GetBlogByUrl(string url)
        {
            var res = await _dataService.GetAsync<BlogViewModel>($@"SELECT CONVERT(VARCHAR(10), S.CreatedOn, 103) as Date,Text1 as Header, Text2 as URL, Text3 as Content,M.FileName
            FROM  CMSContents S
            JOIN Medias M on M.Id=S.MediaId
            Where S.IsActive=1 and S.Text2 ='{url}'");
            return res;
        }

        public async Task<IEnumerable<BlogViewModel>> GetBlogs(int rowCount = 0)
        {
            string strCount = "";
            if (rowCount > 0)
            {
                strCount = $" Top({rowCount}) ";
            }

            var res = await _dataService.GetEnumerableAsync<BlogViewModel>($@"SELECT {strCount} CONVERT(VARCHAR(12), s.CreatedOn, 113) as Date,Text1 as Header, Text2 as URL, Text3 as Content,M.FileName
            FROM  CMSContents S
            JOIN Medias M on M.Id=S.MediaId
            Where S.IsActive=1 and S.CMSKey ='blog'
            order by s.CreatedOn desc");
            return res;
        }

        public async Task<IEnumerable<NewsViewModel>> GetNews(int rowCount = 0)
        {
            string strCount = "";
            if (rowCount > 0)
            {
                strCount = $" Top({rowCount}) ";
            }

            var res = await _dataService.GetEnumerableAsync<NewsViewModel>($@"SELECT {strCount} CONVERT(VARCHAR(12), s.CreatedOn, 113) as Date,Text1 as Header, Text2 as URL, Text3 as Content,M.FileName
            FROM  CMSContents S
            JOIN Medias M on M.Id=S.MediaId
            Where S.IsActive=1 and S.CMSKey ='News'
            order by s.CreatedOn desc");
            return res;
        }
    }
}
