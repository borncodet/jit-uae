﻿
using GymManagment.Core.PostModels;
using GymManagment.Core.ViewModels;
using GymManagment.Repository.Interfaces;
using GymManagment.Shared.ViewModels;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GymManagment.Repository
{

	public interface IWorkoutRepository
	{
		Task<WorkoutLoadViewModel> OnLoad(string workoutId);
		Task<ResponseInfo> Save(WorkoutPostModel model, string userId);
		Task<WorkoutChartViewModel> WorkoutChart(string workoutId);
	}


	public class WorkoutRepository : IWorkoutRepository
	{
		private readonly IDataService _dataService;

		public WorkoutRepository(IDataService dataService)
		{
			_dataService = dataService;
		}


		public async Task<WorkoutLoadViewModel> OnLoad(string workoutId)
		{
			var res = new WorkoutLoadViewModel
			{
				ActivityList = await GetActivities(),
				ExcercisList = await GetExcercises(),
				IntensityList = await GetIntensities(),
				TimeList = GetTimes()
			};
			if (!string.IsNullOrEmpty(workoutId))
			{
				res.Master = await GetMaster(workoutId);
				res.Warmups = await GetWarmups(workoutId);
				res.Strengths = await GetStrengths(workoutId);
				res.Cooldowns = await GetCooldowns(workoutId);
				res.Cardios = await GetCardios(workoutId);
			}
			return res;
		}


		public async Task<WorkoutChartViewModel> WorkoutChart(string workoutId)
		{
			var res = new WorkoutChartViewModel
			{
				Master = await GetMaster(workoutId),
				MinAndMaxDay = await GetMinAndMaxDay(workoutId),
				Warmups = await GetWarmups(workoutId),
				Strengths = await GetStrengths(workoutId),
				Cooldowns = await GetCooldowns(workoutId),
				Cardios = await GetCardios(workoutId)
			};
			return res;
		}


		public async Task<ResponseInfo> Save(WorkoutPostModel model, string userId)
		{
			var result = new ResponseInfo();

			#region Set Activity and Excersice Ids

			foreach (var item in model.Warmups)
			{
				item.ActivityId = await GetActivityId(item.Activity);
			}
			foreach (var item in model.Cooldowns)
			{
				item.ActivityId = await GetActivityId(item.Activity);
			}
			foreach (var item in model.Cardios)
			{
				item.ExcerciseId = await GetExcerciseId(item.Excercise);
			}
			foreach (var item in model.Strengths)
			{
				item.ExcerciseId = await GetExcerciseId(item.Excercise);
			}

			#endregion

			var cn = _dataService.GetDbConnection();
			cn.Open();
			using (var tran = cn.BeginTransaction())
			{
				try
				{
					if (string.IsNullOrEmpty(model.Id))
					{
						model.Id = await _dataService.ExecuteScalarAsync($@"INSERT INTO Workouts
						(Id, CreatedBy, CreatedOn, IsActive, CustomerId, FromDate, ToDate, Goal)
						OUTPUT inserted.Id 
						VALUES (NEWID(),'{userId}',GETDATE(), 1, @CustomerId,@FromDate,@ToDate,@Goal)", model, transaction: tran);
					}
					else
					{
						await _dataService.ExecuteScalarAsync($@"Update Workouts
						Set UpdatedBy='{userId}', UpdatedOn=GETDATE(), CustomerId=@CustomerId, FromDate=@FromDate, ToDate=@ToDate, Goal=@Goal
						Where Id=@Id", model, transaction: tran);

						await _dataService.ExecuteAsync($@"Delete From Workout_Warmups Where WorkoutId=@Id",model, tran);
						await _dataService.ExecuteAsync($@"Delete From Workout_Strengths Where WorkoutId=@Id", model, tran);
						await _dataService.ExecuteAsync($@"Delete From Workout_Cardios Where WorkoutId=@Id", model, tran);
						await _dataService.ExecuteAsync($@"Delete From Workout_Cooldowns Where WorkoutId=@Id", model, tran);
					}

					await _dataService.ExecuteAsync($@"INSERT INTO Workout_Warmups(Id, CreatedBy, CreatedOn, IsActive,  WorkoutId, DayFrom, DayTo, ActivityId, TimeDist, SetsReps, IntensityId, Notes)
					Values(NEWID(),'{userId}',GETDATE(),1,'{model.Id}',@DayFrom, @DayTo, @ActivityId,@TimeDist,@SetsReps,@IntensityId,@Notes)", model.Warmups, tran);

					await _dataService.ExecuteAsync($@"INSERT INTO Workout_Strengths(Id, CreatedBy, CreatedOn, IsActive,  WorkoutId, DayFrom, DayTo, ExcerciseId, SetsReps, Weight, RestTime, Notes)
					Values(NEWID(),'{userId}',GETDATE(),1,'{model.Id}',@DayFrom, @DayTo, @ExcerciseId, @SetsReps, @Weight, @RestTime, @Notes)", model.Strengths, tran);

					await _dataService.ExecuteAsync($@"INSERT INTO Workout_Cardios(Id, CreatedBy, CreatedOn, IsActive,  WorkoutId, DayFrom, DayTo, ExcerciseId, TimeDist, TargetHR, IntensityId, Notes)
					Values(NEWID(),'{userId}',GETDATE(),1,'{model.Id}', @DayFrom, @DayTo, @ExcerciseId, @TimeDist, @TargetHR, @IntensityId, @Notes)", model.Cardios, tran);

					await _dataService.ExecuteAsync($@"INSERT INTO  Workout_Cooldowns(Id, CreatedBy, CreatedOn, IsActive,  WorkoutId, DayFrom, DayTo, ActivityId, TimeDist, SetsReps, IntensityId, Notes)
					Values(NEWID(),'{userId}',GETDATE(),1,'{model.Id}', @DayFrom, @DayTo, @ActivityId, @TimeDist, @SetsReps, @IntensityId, @Notes)", model.Cooldowns, tran);

					tran.Commit();
					result.CreateSuccessResponse(model.Id);
				}
				catch (Exception err)
				{
					tran.Rollback();
					result.CreateFailureResponse(code: -1, message: err.Message);
				}
			}
			return result;
		}


		#region Private Functions

		private async Task<IEnumerable<string>> GetActivities()
		{
			return await _dataService.GetEnumerableAsync<string>($@"SELECT Name
			FROM Workout_Activities");
		}

		private async Task<IEnumerable<string>> GetExcercises()
		{
			return await _dataService.GetEnumerableAsync<string>($@"SELECT Name
			FROM Workout_Excercises");
		}

		private IEnumerable<string> GetTimes()
		{
			List<string> Times = new List<string>();
			for (int i = 1; i <= 60; i++)
			{
				Times.Add(i + " MIN");
			}
			return Times;
		}

		private async Task<IEnumerable<IdValuePair>> GetIntensities()
		{
			return await _dataService.GetEnumerableAsync<IdValuePair>($@"SELECT Id, Name as Value
			FROM Workout_Intensities");
		}


		private async Task<string> GetActivityId(string name)
		{
			string id = await _dataService.GetAsync<string>($@"SELECT Id FROM Workout_Activities where Name='{name}'");
			if (string.IsNullOrEmpty(id))
			{
				id = await _dataService.ExecuteScalarAsync($@"INSERT INTO Workout_Activities
					(Id, Name)
					OUTPUT inserted.Id 
					VALUES (NEWID(),'{name}')");
			}
			return id;
		}

		private async Task<string> GetExcerciseId(string name)
		{
			string id = await _dataService.GetAsync<string>($@"SELECT Id FROM Workout_Excercises where Name='{name}'");
			if (string.IsNullOrEmpty(id))
			{
				id = await _dataService.ExecuteScalarAsync($@"INSERT INTO Workout_Excercises
					(Id, Name)
					OUTPUT inserted.Id 
					VALUES (NEWID(),'{name}')");
			}
			return id;
		}


		private async Task<WorkoutMasterViewModel> GetMaster(string workoutId)
		{
			return await _dataService.GetAsync<WorkoutMasterViewModel>($@"SELECT  W.Id, CustomerId, FromDate, ToDate, Goal,C.MembershipNo,isnull(C.FirstName,'')+' '+ISNULL(C.LastName,'') as Name
			FROM Workouts W
			JOIN Customers C on W.CustomerId=C.Id
			Where W.Id='{workoutId}'");
		}

		private async Task<IEnumerable<Workout_WarmupPostModel>> GetWarmups(string workoutId)
		{
			return await _dataService.GetEnumerableAsync<Workout_WarmupPostModel>($@"SELECT W.Id, WorkoutId, ROW_NUMBER() OVER (ORDER BY  DayFrom) AS SlNO, DayFrom, DayTo, 
			ActivityId, TimeDist, SetsReps, IntensityId, Notes, I.Name as Intensity,A.Name as Activity
			FROM  Workout_Warmups W
			LEFT JOIN Workout_Intensities I on I.Id=W.IntensityId
			LEFT JOIN Workout_Activities A on A.Id=W.ActivityId
			Where W.IsActive=1 and W.WorkoutId='{workoutId}'");
		}

		private async Task<IEnumerable<Workout_StrengthPostModel>> GetStrengths(string workoutId)
		{
			return await _dataService.GetEnumerableAsync<Workout_StrengthPostModel>($@"SELECT W.Id, WorkoutId, ROW_NUMBER() OVER (ORDER BY  DayFrom) AS SlNO, DayFrom, DayTo, ExcerciseId, SetsReps, Weight, RestTime, Notes, I.Name as Excercise
			FROM  Workout_Strengths W
			LEFT JOIN Workout_Excercises I on I.Id=W.ExcerciseId
			Where W.IsActive=1 and W.WorkoutId='{workoutId}'");
		}

		private async Task<IEnumerable<Workout_CardioPostModel>> GetCardios(string workoutId)
		{
			return await _dataService.GetEnumerableAsync<Workout_CardioPostModel>($@"SELECT W.Id, WorkoutId, ROW_NUMBER() OVER (ORDER BY  DayFrom) AS SlNO, DayFrom, DayTo,  ExcerciseId, TimeDist, TargetHR, IntensityId, Notes, E.Name as Excercise,I.Name as Intensity
			FROM  Workout_Cardios W
			LEFT JOIN Workout_Excercises E on E.Id=W.ExcerciseId
			LEFT JOIN Workout_Intensities I on I.Id=W.IntensityId
			Where W.IsActive=1 and W.WorkoutId='{workoutId}'");
		}

		private async Task<IEnumerable<Workout_CooldownPostModel>> GetCooldowns(string workoutId)
		{
			return await _dataService.GetEnumerableAsync<Workout_CooldownPostModel>($@"SELECT W.Id, WorkoutId, ROW_NUMBER() OVER (ORDER BY  DayFrom) AS SlNO, DayFrom, DayTo, ActivityId, TimeDist, SetsReps, IntensityId, Notes, A.Name as Activity,I.Name as Intensity
			FROM  Workout_Cooldowns W
			LEFT JOIN Workout_Activities A on A.Id=W.ActivityId
			LEFT JOIN Workout_Intensities I on I.Id=W.IntensityId
			Where W.IsActive=1 and W.WorkoutId='{workoutId}'");
		}

		private async Task<WorkoutMinMaxDayViewModel> GetMinAndMaxDay(string workoutId)
		{
			return await _dataService.GetAsync<WorkoutMinMaxDayViewModel>($@"Select Min(MinDay) MinDay,Max(MaxDay) MaxDay
			From(
				SELECT MIN(DayFrom) MinDay,Max(DayTo) MaxDay From Workout_Warmups Where WorkoutId='{workoutId}'
				UNION
				SELECT MIN(DayFrom) MinDay,Max(DayTo) MaxDay From Workout_Strengths Where WorkoutId='{workoutId}'
				UNION
				SELECT MIN(DayFrom) MinDay,Max(DayTo) MaxDay From Workout_Cardios Where WorkoutId='{workoutId}'
				UNION
				SELECT MIN(DayFrom) MinDay,Max(DayTo) MaxDay From Workout_Cooldowns Where WorkoutId='{workoutId}'
			)as A");
		}

		#endregion
	}
}
