﻿using System;

namespace GymManagment.Shared.ViewModels
{
	public class ResponseInfo
	{
		public int ResponseCode { get; set; }
		public string Response { get; set; }

		private bool _isError;
		public bool IsError
		{
			get
			{
				if (ResponseCode < 0)
				{
					_isError = true;
				}
				else
				{
					_isError = false;
				}
				return _isError;
			}
			set { _isError = value; }
		}

		private string _responseDescription;

		public string ResponseDescription
		{
			get
			{
				if (String.IsNullOrEmpty(_responseDescription))
				{
					_responseDescription = Response;
				}
				return _responseDescription;
			}
			set { _responseDescription = value; }
		}

		public ResponseInfo()
		{
		}

		//public ResponseInfo(bool isError, int code, string response, string responseDesc)
		//{
		//	IsError = isError;
		//	ResponseCode = code;
		//	Response = response;
		//	ResponseDescription = responseDesc;
		//}

		public virtual void CreateSuccessResponse(string message = null)
		{
			IsError = false;
			ResponseCode = 0;
			Response = string.IsNullOrEmpty(message) ? $"The request has been completed successfully." : message;
			ResponseDescription = "";
		}

		public virtual void CreateFailureResponse(string message= "", int? code = null, string responseDesc = "")
		{
			IsError = true;
			ResponseCode = code == null ? -1 : (int)code;
			Response = string.IsNullOrEmpty(message) ? $"Error occured" : message;
			ResponseDescription = responseDesc;

		}
	}
}
