﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GymManagment.Shared.Interfaces
{
	public interface IActiveEntity
	{
		bool IsActive { get; set; }
	}
}
