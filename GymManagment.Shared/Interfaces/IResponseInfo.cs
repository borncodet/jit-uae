﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GymManagment.Shared.Interfaces
{
	public interface IResponseInfo<T>
	{
		T Response { get; set; }
	}
}
