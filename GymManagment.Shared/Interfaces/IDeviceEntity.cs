﻿using GymManagment.Shared.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace GymManagment.Shared.Interfaces
{
	public interface IDeviceEntity
	{
		DeviceType DeviceType { get; set; }
		string DeviceId { get; set; }
		string DeviceName { get; set; }
	}
}
