﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GymManagment.Shared.Enums
{
	public enum ResponseType
	{
		Success = 1,
		Error,
		Info,
		Warning
	}
}
