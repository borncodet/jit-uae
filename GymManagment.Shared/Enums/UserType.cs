﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;

namespace GymManagment.Shared.Enums
{
	public enum UserTypeIds
	{
		Admin = 1,
		Trainer = 2,
		FrontDesk = 3,
	}

	public	class UserTypeItem
	{
		public int Value { get; set; }
		public string Text { get; set; }
	}
	
}
