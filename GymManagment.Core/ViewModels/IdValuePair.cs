﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GymManagment.Core.ViewModels
{
	public class IdValuePair
	{
		public string Id { get; set; }
		public string Value { get; set; }
	}
}
