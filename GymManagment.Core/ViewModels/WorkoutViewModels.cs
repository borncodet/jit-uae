﻿using GymManagment.Core.PostModels;
using GymManagment.Shared.Interfaces;
using GymManagment.Shared.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace GymManagment.Core.ViewModels
{
	public class WorkoutLoadViewModel
	{
		public IEnumerable<string> ActivityList { get; set; }
		public IEnumerable<string> ExcercisList { get; set; }
		public IEnumerable<string> TimeList { get; set; }
		public IEnumerable<IdValuePair> IntensityList { get; set; }

		public WorkoutMasterViewModel Master { get; set; }
		public IEnumerable<Workout_WarmupPostModel> Warmups { get; set; }
		public IEnumerable<Workout_StrengthPostModel> Strengths { get; set; }
		public IEnumerable<Workout_CardioPostModel> Cardios { get; set; }
		public IEnumerable<Workout_CooldownPostModel> Cooldowns { get; set; }
	}

	public class WorkoutSaveViewModel : IResponseInfo<ResponseInfo>
	{
		public ResponseInfo Response { get; set; }

		public WorkoutSaveViewModel()
		{
			this.Response = new ResponseInfo();
		}
	}


	public class WorkoutMasterViewModel
	{
		public string Id { get; set; }
		public string CustomerId { get; set; }
		public DateTime FromDate { get; set; }
		public DateTime ToDate { get; set; }
		public string Goal { get; set; }
		public int MembershipNo { get; set; }
		public string Name { get; set; }
	}

	public class WorkoutChartViewModel
	{
		public WorkoutMasterViewModel Master { get; set; }
		public WorkoutMinMaxDayViewModel MinAndMaxDay { get; set; }
		public IEnumerable<Workout_WarmupPostModel> Warmups { get; set; }
		public IEnumerable<Workout_StrengthPostModel> Strengths { get; set; }
		public IEnumerable<Workout_CardioPostModel> Cardios { get; set; }
		public IEnumerable<Workout_CooldownPostModel> Cooldowns { get; set; }
	}

	public class WorkoutMinMaxDayViewModel
	{
		public int MinDay { get; set; }
		public int MaxDay { get; set; }
	}
}
