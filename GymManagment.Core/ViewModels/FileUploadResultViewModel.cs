﻿using GymManagment.Shared.Interfaces;
using GymManagment.Shared.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace GymManagment.Core.ViewModels
{
	public class FileUploadResultViewModel : ISuccess<UploadFilePostInfo>, IResponseInfo<ResponseInfo>
	{
		public UploadFilePostInfo Result { get; set; }
		public ResponseInfo Response { get; set; }

		public FileUploadResultViewModel()
		{
			this.Result = new UploadFilePostInfo();
			this.Response = new ResponseInfo();
		}
	}



	public class UploadFilePostModel
	{
		public string FileName { get; set; }
		public string Extension { get; set; }
		public string ContentType { get; set; }
		public long ContentLength { get; set; }
	}

	public class UploadFilePostInfo
	{
		public string MediaId { get; set; }
	}
}
