﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GymManagment.Core.ViewModels
{
	public class ImagesSaveViewModel
	{
		public bool IsSuccess { get; set; }
		public string MediaID { get; set; }
	}
}
