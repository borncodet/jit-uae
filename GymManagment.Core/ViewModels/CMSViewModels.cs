﻿using GymManagment.Core.Models;
using System.Collections.Generic;

namespace GymManagment.Core.ViewModels
{
    public class CMSContentViewModel
    {
        public string Key { get; set; }
        public string Text1 { get; set; }
        public string Text2 { get; set; }
        public string Text3 { get; set; }
        public string FileName { get; set; }
    }

    public class CMSContentSocialViewModel
    {
        public IEnumerable<CMSContentViewModel> CMSContentViewModel { get; set; }
        public GeneralSettings SocialLinks { get; set; }
    }

    public class BlogViewModel
    {
        public string Header { get; set; }
        public string URL { get; set; }
        public string Content { get; set; }
        public string FileName { get; set; }
        public string Date { get; set; }
    }

    public class NewsViewModel
    {
        public string Header { get; set; }
        public string URL { get; set; }
        public string Content { get; set; }
        public string FileName { get; set; }
        public string Date { get; set; }
    }
}
