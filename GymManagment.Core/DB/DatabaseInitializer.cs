﻿using GymManagment.Core.DB.Interface;
using GymManagment.Core.Interfaces;
using GymManagment.Core.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Threading.Tasks;

namespace GymManagment.Core.DB
{
    public class DatabaseInitializer : IDatabaseInitializer
    {
        private readonly ApplicationDbContext _context;
        private readonly IAccountManager _accountManager;
        //private readonly ILogger _logger;

        public DatabaseInitializer(ApplicationDbContext context, IAccountManager accountManager)
        {
            _context = context;
            _accountManager = accountManager;
            //_logger = logger;
        }

        public virtual async Task SeedAsync()
        {
            await _context.Database.MigrateAsync().ConfigureAwait(false);

            if (!await _context.Roles.AnyAsync())
            {
                //_logger.LogInformation("Generating inbuilt roles");

                await CreateRolesAsync("SuperAdministrator", "", new string[] { });
                await CreateRolesAsync("Administrator", "", new string[] { });
                await CreateRolesAsync("Trainer", "", new string[] { });
                await CreateRolesAsync("FrontDesk", "", new string[] { });

                //_logger.LogInformation("Inbuilt roles creation completed");
            }

            if (!await _context.Users.AnyAsync())
            {
                //_logger.LogInformation("Generating inbuilt accounts");

                await CreateUserAsync("admin@gmail.com", "admin@2019", "Administrator", "", "admin@gmail.com", "", new string[] { }, 1);

                //_logger.LogInformation("Inbuilt account generation completed");
            }

            if (!await _context.Workout_Intensities.AnyAsync())
            {
                _context.Workout_Intensities.Add(new Workout_Intensity()
                {
                    Id = Guid.NewGuid().ToString(),
                    Name = "Low",
                });
                _context.Workout_Intensities.Add(new Workout_Intensity()
                {
                    Id = Guid.NewGuid().ToString(),
                    Name = "Medium",
                });
                _context.Workout_Intensities.Add(new Workout_Intensity()
                {
                    Id = Guid.NewGuid().ToString(),
                    Name = "High",
                });
                _context.SaveChanges();
            }

            if (!await _context.Countries.AnyAsync())
            {
                _context.Countries.Add(new Country()
                {
                    Id = "1",
                    Name = "India",
                    ISDCode = "+91",
                    IsActive = true
                });
                _context.SaveChanges();
            }

            if (!await _context.States.AnyAsync())
            {
                _context.States.Add(new State()
                {
                    Id = "1",
                    Name = "Kerala",
                    CountryId = "1",
                    IsActive = true
                });
                _context.SaveChanges();
            }
        }

        private async Task<ApplicationUser> CreateUserAsync(string userName, string password, string firstName, string lastName, string email, string phoneNumber, string[] roles, int userTypeId)
        {
            ApplicationUser applicationUser = new ApplicationUser
            {
                UserName = userName,
                FirstName = firstName,
                LastName = lastName,
                Email = email,
                PhoneNumber = phoneNumber,
                EmailConfirmed = true,
                PhoneNumberConfirmed = true,
                IsActive = true,
                UserTypeId = userTypeId
            };

            var (Succeeded, Errors) = await _accountManager.CreateUserAsync(applicationUser, roles, password);

            if (!Succeeded)
            {
                throw new Exception($"Seeding \"{userName}\" user failed. Errors: {string.Join(Environment.NewLine, Errors)}");
            }

            return applicationUser;
        }

        private async Task CreateRolesAsync(string roleName, string description, string[] claims)
        {
            ApplicationRole applicationRole = new ApplicationRole(roleName, description);
            await this._accountManager.CreateRoleAsync(applicationRole, claims);
        }

    }
}
