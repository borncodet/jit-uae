﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace GymManagment.Core.DB.Interface
{
	public interface IDatabaseInitializer
	{
		Task SeedAsync();
	}
}
