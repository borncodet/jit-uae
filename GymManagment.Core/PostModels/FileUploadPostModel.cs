﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace GymManagment.Core.PostModels
{
	public class FileUploadPostModel
	{
		//[Required]
		public IList<IFormFile> files { get; set; }
		[Required]
		public ImageType ImageType { get; set; }
		//[Required]
		//public string MachineId { get; set; }
	}

	public enum ImageType
	{
		MemberPhoto=1,
		Bill=2
	}
}
