﻿using Microsoft.AspNetCore.Http;
using System.ComponentModel.DataAnnotations;

namespace GymManagment.Core.PostModels
{
    public class CMSContentViewPostModel
    {
        public string Id { get; set; }
        public string CMSKey { get; set; }
        public string Text1 { get; set; }
        public string Text2 { get; set; }
        [DataType(DataType.MultilineText)]
        public string Text3 { get; set; }
        public string MediaId { get; set; }
        public bool IsActive { get; set; }

        public IFormFile Image { get; set; }
    }

}
