﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace GymManagment.Core.PostModels
{
	public class WorkoutPostModel
	{
		public string Id { get; set; }
		[Required]
		public string CustomerId { get; set; }
		[Required]
		public DateTime FromDate { get; set; }
		[Required]
		public DateTime ToDate { get; set; }
		public string Goal { get; set; }

		public List<Workout_WarmupPostModel> Warmups { get; set; }
		public List<Workout_StrengthPostModel> Strengths { get; set; }
		public List<Workout_CardioPostModel> Cardios { get; set; }
		public List<Workout_CooldownPostModel> Cooldowns { get; set; }
		
	}

	public class Workout_WarmupPostModel
	{
		public string Id { get; set; }
		[Required]
		public int DayFrom { get; set; }
		[Required]
		public int DayTo { get; set; }
		[Required]
		public string Activity { get; set; }
		[Required]
		public string TimeDist { get; set; }
		[Required]
		public string SetsReps { get; set; }
		[Required]
		public string IntensityId { get; set; }
		public string Notes { get; set; }

		//For later settlement
		public string ActivityId { get; set; }


		//For ViewModel
		public string Intensity { get; set; }
		public int SlNo { get; set; }
	}


	public class Workout_CardioPostModel
	{
		public string Id { get; set; }
		[Required]
		public int DayFrom { get; set; }
		[Required]
		public int DayTo { get; set; }
		[Required]
		public string Excercise { get; set; }
		[Required]
		public string TimeDist { get; set; }
		[Required]
		public string TargetHR { get; set; }
		[Required]
		public string IntensityId { get; set; }
		public string Notes { get; set; }

		//For later settlement
		public string ExcerciseId { get; set; }

		//For ViewModel
		public string Intensity { get; set; }
		public int SlNo { get; set; }
	}

	public class Workout_CooldownPostModel
	{
		public string Id { get; set; }
		[Required]
		public int DayFrom { get; set; }
		[Required]
		public int DayTo { get; set; }
		[Required]
		public string Activity { get; set; }
		[Required]
		public string TimeDist { get; set; }
		[Required]
		public string SetsReps { get; set; }
		[Required]
		public string IntensityId { get; set; }
		public string Notes { get; set; }


		//For later settlement
		public string ActivityId { get; set; }

		//For ViewModel
		public string Intensity { get; set; }
		public int SlNo { get; set; }
	}

	public class Workout_StrengthPostModel
	{
		public string Id { get; set; }
		[Required]
		public int DayFrom { get; set; }
		[Required]
		public int DayTo { get; set; }
		[Required]
		public string Excercise { get; set; }
		[Required]
		public string SetsReps { get; set; }
		[Required]
		public string Weight { get; set; }
		[Required]
		public string RestTime { get; set; }
		public string Notes { get; set; }

		//For later settlement
		public string ExcerciseId { get; set; }
		public int SlNo { get; set; }
	}

	
}
