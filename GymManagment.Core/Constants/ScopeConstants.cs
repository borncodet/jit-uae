﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GymManagment.Core.Constants
{
	public static class ScopeConstants
	{
		///<summary>
		///A scope that specifies the roles of an entity
		///</summary>
		public const string Roles = "roles";
	}
}
