﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GymManagment.Core.Constants
{
	public static class PropertyConstants
	{
		public const string FirstName = "firstname";

		public const string LastName = "lastname";

		public const string Configuration = "configuration";

		public const string ExtensionData = "extensiondata";
	}
}
