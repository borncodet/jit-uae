﻿using System.ComponentModel.DataAnnotations.Schema;

namespace GymManagment.Core.Models
{
	[Table(name: "Workout_Cardios")]
	public class Workout_Cardio : BaseEntity
	{
		public string WorkoutId { get; set; }
		public int DayFrom { get; set; }
		public int DayTo { get; set; }
		public string ExcerciseId { get; set; }
		public string TimeDist { get; set; }
		public string TargetHR { get; set; }
		public string IntensityId { get; set; }
		public string Notes { get; set; }

		public virtual Workout Workout { get; set; }
		public virtual Workout_Excercise Excercise { get; set; }
		public virtual Workout_Intensity Intensity { get; set; }

	}
}
