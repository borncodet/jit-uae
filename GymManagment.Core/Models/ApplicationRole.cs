﻿using GymManagment.Shared.Interfaces;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;

namespace GymManagment.Core.Models
{
	public class ApplicationRole : IdentityRole, IAuditableEntity
	{
		public ApplicationRole()
		{

		}

		public ApplicationRole(string roleName) : base(roleName)
		{

		}

		public ApplicationRole(string roleName, string description) : base(roleName)
		{
			Description = description;
		}

		public string Description { get; set; }

		public string CreatedBy { get; set; }
		public DateTime CreatedOn { get; set; }
		public string UpdatedBy { get; set; }
		public DateTime? UpdatedOn { get; set; }

		public virtual ICollection<IdentityUserRole<string>> Users { get; set; }
		public virtual ICollection<IdentityRoleClaim<string>> Claims { get; set; }
	}
}
