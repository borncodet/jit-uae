﻿using System;
using System.Collections.Generic;

namespace GymManagment.Core.Models
{
    public class Workout : BaseEntity
    {
        public string CustomerId { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public string Goal { get; set; }

        public virtual ICollection<Workout_Warmup> Workout_Warmups { get; set; }
        public virtual ICollection<Workout_Cooldown> Workout_Cooldowns { get; set; }
        public virtual ICollection<Workout_Strength> Workout_Strengths { get; set; }
        public virtual ICollection<Workout_Cardio> Workout_Cardios { get; set; }
    }
}
