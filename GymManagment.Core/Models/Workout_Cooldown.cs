﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace GymManagment.Core.Models
{
	[Table(name: "Workout_Cooldowns")]
	public class Workout_Cooldown : BaseEntity
	{
		public string WorkoutId { get; set; }
		public int DayFrom { get; set; }
		public int DayTo { get; set; }
		public string ActivityId { get; set; }
		public string TimeDist { get; set; }
		public string SetsReps { get; set; }
		public string IntensityId { get; set; }
		public string Notes { get; set; }

		public virtual Workout Workout { get; set; }
		public virtual Workout_Activity Activity { get; set; }
		public virtual Workout_Intensity Intensity { get; set; }
	}
}
