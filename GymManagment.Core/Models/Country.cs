﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace GymManagment.Core.Models
{
	public class Country : BaseEntity
	{
		[Required]
		public string Name { get; set; }
		[Display(Name="ISD Code")]
		public string ISDCode { get; set; }
	}
}
