﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GymManagment.Core.Models
{
	public class CMSContent:BaseEntity
	{
		public string CMSKey { get; set; }
		public string Text1 { get; set; }
		public string Text2 { get; set; }
		public string Text3 { get; set; }
		public string MediaId { get; set; }
		public Media Media { get; set; }
	}
}
