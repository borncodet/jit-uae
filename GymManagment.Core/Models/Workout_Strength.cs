﻿using System.ComponentModel.DataAnnotations.Schema;

namespace GymManagment.Core.Models
{
	[Table(name: "Workout_Strengths")]
	public class Workout_Strength : BaseEntity
	{
		public string WorkoutId { get; set; }
		public int DayFrom { get; set; }
		public int DayTo { get; set; }
		public string ExcerciseId { get; set; }
		public string SetsReps { get; set; }
		public string Weight { get; set; }
		public string RestTime { get; set; }
		public string Notes { get; set; }

		public virtual Workout Workout { get; set; }
		public virtual Workout_Excercise Excercise { get; set; }
	}
}
