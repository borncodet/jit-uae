﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace GymManagment.Core.Models
{
	[Table(name: "Workout_Excercises")]
	public class Workout_Excercise
	{
		[Key, Column(Order = 0)]
		[MaxLength(256)]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public string Id { get; set; }

		public string Name { get; set; }

		public virtual ICollection<Workout_Cardio> Cardios { get; set; }
		public virtual ICollection<Workout_Strength> Strengths { get; set; }
	}
}
