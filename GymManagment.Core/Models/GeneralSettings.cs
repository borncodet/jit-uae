﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GymManagment.Core.Models
{
	public class GeneralSettings:BaseEntity
	{
		public string CompanyName { get; set; }
		public string Address1 { get; set; }
		public string Address2 { get; set; }
		public string Address3 { get; set; }
		public string ContactNo { get; set; }
		public string Website { get; set; }
		public string EmailAddress { get; set; }
		public string InvoiceFooterText { get; set; }
		public string BalanceInvoiceFooterText { get; set; }
		public int AttendanceImportedUptoId { get; set; }
		public string AboutUs { get; set; }
		public string Twitter { get; set; }
		public string Facebook { get; set; }
		public string Instagram { get; set; }
	}
}
