﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace GymManagment.Core.Models
{
	[Table(name: "Workout_Intensities")]
	public class Workout_Intensity
	{
		[Key, Column(Order = 0)]
		[MaxLength(256)]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public string Id { get; set; }

		public string Name { get; set; }

		public virtual IEnumerable<Workout_Warmup> Workout_Warmups { get; set; }
		public virtual IEnumerable<Workout_Cardio> Workout_Cardios { get; set; }
		public virtual IEnumerable<Workout_Cooldown> Workout_Cooldowns { get; set; }
	}
}
