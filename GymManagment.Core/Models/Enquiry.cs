﻿using System.ComponentModel.DataAnnotations;

namespace GymManagment.Core.Models
{
    public class Enquiry : BaseEntity
    {
        //[Required]
        [Display(Name = "Name Prefix")]
        public int NamePrefixId { get; set; }
        public virtual NamePrefix NamePrefix { get; set; }

        [Required]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        //[Required]
        [Display(Name = "Gender")]
        public int GenderId { get; set; }
        public virtual Gender Gender { get; set; }

        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }

        [Required]
        [Phone]
        [Display(Name = "Mobile Number")]
        public string MobileNo { get; set; }

        [EmailAddress]
        public string Email { get; set; }

        public bool IsFromOnline { get; set; }
        public bool IsContacted { get; set; }
        public string Message { get; set; }
    }
}
