﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GymManagment.Core.PostModels;
using GymManagment.Core.ViewModels;
using GymManagment.Repository;
using GymManagment.Repository.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace GymManagment.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class WorkoutController : BaseController
	{
		private readonly IWorkoutRepository _workoutRepository;

		public WorkoutController(IWorkoutRepository workoutRepository,
			IHttpContextAccessor httpContextAccessor,
			ILogger<BaseController> logger) :base(httpContextAccessor,logger)
		{
			_workoutRepository = workoutRepository;
		}

		[Route("onload")]
		[HttpPost]
		[ProducesResponseType(typeof(WorkoutLoadViewModel), 200)]
		public async Task<WorkoutLoadViewModel> OnLoad([FromBody]string	workoutId)
		{
			return await  _workoutRepository.OnLoad(workoutId);
		}

		[HttpPost]
		[Route("save")]
		public async Task<WorkoutSaveViewModel> Save(WorkoutPostModel model)
		{
			var response = new WorkoutSaveViewModel
			{
				Response = await _workoutRepository.Save(model, GetCurrentUserId())
			};
			return response;
		}

		[Route("chart")]
		[HttpPost]
		[ProducesResponseType(typeof(WorkoutChartViewModel), 200)]
		public async Task<WorkoutChartViewModel> Chart([FromBody]string workoutId)
		{
			return await _workoutRepository.WorkoutChart(workoutId);
		}
	}
}