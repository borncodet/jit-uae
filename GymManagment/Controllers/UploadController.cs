﻿using GymManagment.Core.PostModels;
using GymManagment.Core.ViewModels;
using GymManagment.Helpers;
using GymManagment.Repository.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Globalization;
using System.IO;
using System.Threading.Tasks;

namespace GymManagment.Controllers
{

	[Route("api/[controller]")]
	[ApiController]
	[Authorize]
	public class UploadController : BaseController
	{
		protected readonly IHostingEnvironment _env;
		private readonly IMediaRepository _media;

		public UploadController(IHostingEnvironment env,
			IHttpContextAccessor accessor,
			ILogger<BaseController> logger,
			IMediaRepository media) : base(accessor, logger)
		{
			_env = env;
			_media = media;
		}

		[HttpPost, DisableRequestSizeLimit]
		[ServiceFilter(typeof(ValidateMimeMultipartContentFilter))]
		[Produces(typeof(FileUploadResultViewModel))]
		[Route("post")]
		public async Task<FileUploadResultViewModel> Post()
		{
			var response = new FileUploadResultViewModel();
			if (!(Request.Form.Files.Count > 0))
			{
				response.Response.Response = "Image not found.";
				response.Response.ResponseDescription = "Please upload image.";
				response.Response.IsError = true;
				return response;
			}
			int imageType = Convert.ToInt32(Request.Form["ImageType"]);
			var file = Request.Form.Files[0];
			string folderName="Gallery/";
			try
			{
				//_logger.LogInformation("upload call received");
				if (file.Length > 0)
				{
					switch (imageType)
					{
						case (int)ImageType.Bill:
							folderName += "Bill/" + CultureInfo.CurrentCulture.DateTimeFormat.GetAbbreviatedMonthName(System.DateTime.Now.Month);
							break;
						case (int)ImageType.MemberPhoto:
							folderName = "Customers";
							break;
					}

					if (!Directory.Exists(Path.Combine("wwwroot", folderName)))
					{
						Directory.CreateDirectory(Path.Combine("wwwroot", folderName));
					}
					string fileId = Guid.NewGuid().ToString("N");
					string extension = Path.GetExtension(file.FileName).Substring(1);
					string fileName = $"{fileId}.{extension}";
					string path = Path.Combine(_env.ContentRootPath, "wwwroot", folderName);
					//_logger.LogInformation($"fileName - {fileName}, folderName - {folderName}, Path -  {path}");
					using (var fs = new FileStream(Path.Combine(path, fileName), FileMode.Create))
					{
						await file.CopyToAsync(fs);
					}

					var mediaId = await _media.Save(GetCurrentUserId(), new UploadFilePostModel()
					{
						FileName = "/" + folderName + "/" + fileName,
						ContentLength = file.Length,
						ContentType = file.ContentType,
						Extension = extension
					});

					response.Response.IsError = false;
					response.Result.MediaId = mediaId;
					//_logger.LogError($"File uplaoded successfully. FileName - {fileName}, Folder - {folderName}");

				}
				else
				{
					response.Response.Response = "File not found.";
					response.Response.ResponseDescription = "Please upload image.";
					response.Response.IsError = true;
				}
			}
			catch (Exception ex)
			{
				//_logger.LogError(ex, $"Error occurred while file uploading");
				response.Response.Response = "Image not found.";
				response.Response.ResponseDescription = "Please upload image.";
				response.Response.IsError = true;
			}
			return response;
		}

	}
}