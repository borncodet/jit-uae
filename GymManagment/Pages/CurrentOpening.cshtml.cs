﻿using GymManagment.Core;
using GymManagment.Core.ViewModels;
using GymManagment.Helpers;
using GymManagment.Repository;
using GymManagment.Shared.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Options;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace GymManagment.Pages
{
    public class CurrentOpeningModel : PageModel
    {
        private readonly ICMSRepository _cmsRepository;
        private readonly ApplicationDbContext _context;
        private readonly IEmailer _emailSender;
        private SmtpConfig _config;

        public IList<NewsViewModel> News { get; set; }
        public CurrentOpeningModel(
            ICMSRepository cmsRepository,
            ApplicationDbContext context,
            IEmailer emailSender,
            IOptions<SmtpConfig> config)
        {
            _cmsRepository = cmsRepository;
            _context = context;
            _emailSender = emailSender;
            _config = config.Value;
        }

        [BindProperty]
        public InputModel Input { get; set; }

        public class InputModel
        {
            [Required]
            public string FirstName { get; set; }
            [Required]
            public string LastName { get; set; }
            public string Address { get; set; }
            public string City { get; set; }
            public string State { get; set; }
            public string PostalCode { get; set; }
            public string Country { get; set; }
            [Required]
            [EmailAddress]
            public string Email { get; set; }
            [Phone]
            [Display(Name = "Phone number")]
            public string PhoneNumber { get; set; }
            public IFormFile Image { get; set; }
        }

        public bool SentMail = false;

        public async Task OnGetAsync()
        {
            News = (await _cmsRepository.GetNews(3)).ToList();
        }

        public async Task<IActionResult> OnPostSaveAsync()
        {
            var img = Input.Image;
            var body = $"{Input.Email} has uploaded the details successfully!<br><b>Address :</b><br>{Input.FirstName} {Input.LastName}";
            if (!string.IsNullOrEmpty(Input.Address) && !string.IsNullOrWhiteSpace(Input.Address))
            {
                body += "<br>" + Input.Address;
            }
            if (!string.IsNullOrEmpty(Input.City) && !string.IsNullOrWhiteSpace(Input.City))
            {
                body += "<br>" + Input.City;
            }

            if (!string.IsNullOrEmpty(Input.State) && !string.IsNullOrWhiteSpace(Input.State))
            {
                body += "<br>" + Input.State;
            }

            if (!string.IsNullOrEmpty(Input.PostalCode) && !string.IsNullOrWhiteSpace(Input.PostalCode))
            {
                body += "<br>" + Input.PostalCode;
            }

            if (!string.IsNullOrEmpty(Input.Country) && !string.IsNullOrWhiteSpace(Input.Country))
            {
                body += "<br>" + Input.Country;
            }

            body += "<br><b>Email :</b>" + Input.Email;
            body += "<br><b>PhoneNumber :</b>" + Input.PhoneNumber;

            (bool success, string errorMsg) result = await _emailSender.SendEmailAsync(Input.Email, Input.Email, _config.Name, _config.EmailAddress, "Account Registration", body, Input.Image);
            if (result.success)
            {
                SentMail = true;
            }
            return Page();
        }
    }
}
