﻿using GymManagment.Core;
using GymManagment.Core.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System.Threading.Tasks;

namespace GymManagment.Pages
{
    public class ContactUsModel : PageModel
    {
        private readonly ApplicationDbContext _context;

        public ContactUsModel(ApplicationDbContext context)
        {
            _context = context;
        }

        public void OnGet()
        {
        }

        [BindProperty]
        public Enquiry Enquiry { get; set; }

        public async Task<IActionResult> OnPostSaveAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            Enquiry enquiry = Enquiry;
            enquiry.IsContacted = true;
            enquiry.IsFromOnline = true;
            enquiry.GenderId = 1;
            enquiry.NamePrefixId = 1;
            _context.Enquiries.Add(enquiry);
            await _context.SaveChangesAsync();
            return RedirectToPage("./Index");
        }
    }
}
