﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GymManagment.Core.ViewModels;
using GymManagment.Repository;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace GymManagment.Pages
{
    public class AboutUsModel : PageModel
    {
		private readonly ICMSRepository _cmsRepository;

		public AboutUsModel(ICMSRepository cmsRepository)
		{
			_cmsRepository = cmsRepository;
		}

		public CMSContentViewModel AboutUs { get; set; }

		public async Task OnGetAsync()
        {
			AboutUs = (await _cmsRepository.GetCMSContent("AboutUs")).ToList().FirstOrDefault();
		}
    }
}