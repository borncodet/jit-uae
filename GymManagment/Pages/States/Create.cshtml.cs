﻿using GymManagment.Core.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Threading.Tasks;

namespace GymManagment.Pages.States
{
	[Authorize]
	public class CreateModel : PageModel
	{
		private readonly GymManagment.Core.ApplicationDbContext _context;

		public CreateModel(GymManagment.Core.ApplicationDbContext context)
		{
			_context = context;
		}

		public IActionResult OnGet()
		{
			ViewData["CountryId"] = new SelectList(_context.Countries, "Id", "Name");
			return Page();
		}

		[BindProperty]
		public State State { get; set; }


		public async Task<IActionResult>
			OnPostAsync()
		{
			if (!ModelState.IsValid)
			{
				return Page();
			}

			_context.States.Add(State);
			await _context.SaveChangesAsync();

			return RedirectToPage("./Index");
		}
	}
}
