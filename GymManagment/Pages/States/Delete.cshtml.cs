﻿using GymManagment.Core.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace GymManagment.Pages.States
{
	[Authorize]
	public class DeleteModel : PageModel
	{
		private readonly GymManagment.Core.ApplicationDbContext _context;

		public DeleteModel(GymManagment.Core.ApplicationDbContext context)
		{
			_context = context;
		}

		[BindProperty]
		public State State { get; set; }

		public async Task<IActionResult> OnGetAsync(string id)
		{
			if (id == null)
			{
				return NotFound();
			}

			State = await _context.States
				.Include(s => s.Country).FirstOrDefaultAsync(m => m.Id == id);

			if (State == null)
			{
				return NotFound();
			}
			return Page();
		}

		public async Task<IActionResult> OnPostAsync(string id)
		{
			if (id == null)
			{
				return NotFound();
			}

			State = await _context.States.FindAsync(id);

			if (State != null)
			{
				_context.States.Remove(State);
				await _context.SaveChangesAsync();
			}

			return RedirectToPage("./Index");
		}
	}
}
