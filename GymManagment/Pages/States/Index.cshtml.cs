﻿using GymManagment.Core.Models;
using GymManagment.Shared.Helpers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System.Linq;
using System.Threading.Tasks;

namespace GymManagment.Pages.States
{
	[Authorize]
	public class IndexModel : PageModel
	{
		private readonly GymManagment.Core.ApplicationDbContext _context;

		[BindProperty(SupportsGet = true)]
		public int CurrentPage { get; set; } = 1;

		[BindProperty(SupportsGet = true)]
		public int Count { get; set; } = 10;

		[BindProperty(SupportsGet = true)]
		public int TotalPages { get; set; }

		[BindProperty(SupportsGet = true)]
		public string SearchString { get; set; }

		public IndexModel(GymManagment.Core.ApplicationDbContext context)
		{
			_context = context;
		}

		public PagedListViewModel<State> State { get; set; }

		public async Task OnGetAsync()
		{
			var pagedList = new PagedList<State>();
			var datas = from m in _context.States
						select m;
			if (!string.IsNullOrEmpty(SearchString))
			{
				datas = datas.Where(s => s.Name.Contains(SearchString));
			}

			State = await pagedList.GetPagedListAsync(datas, CurrentPage, Count);
			CurrentPage = (State).PageIndex;
			TotalPages = (State).TotalPages;
		}
	}
}

