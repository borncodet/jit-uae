﻿using GymManagment.Core.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace GymManagment.Pages.States
{
	[Authorize]
	public class DetailsModel : PageModel
	{
		private readonly GymManagment.Core.ApplicationDbContext _context;

		public DetailsModel(GymManagment.Core.ApplicationDbContext context)
		{
			_context = context;
		}

		public State State { get; set; }

		public async Task<IActionResult> OnGetAsync(string id)
		{
			if (id == null)
			{
				return NotFound();
			}

			State = await _context.States
				.Include(s => s.Country).FirstOrDefaultAsync(m => m.Id == id);

			if (State == null)
			{
				return NotFound();
			}
			return Page();
		}
	}
}
