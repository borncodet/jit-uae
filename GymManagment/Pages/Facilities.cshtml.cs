﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GymManagment.Core;
using GymManagment.Core.ViewModels;
using GymManagment.Repository;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace GymManagment.Pages
{
    public class FacilitiesModel : PageModel
    {
		private readonly ICMSRepository _cmsRepository;
		private readonly ApplicationDbContext _context;

		public FacilitiesModel(ICMSRepository cmsRepository, ApplicationDbContext context)
		{
			_cmsRepository = cmsRepository;
			_context = context;
		}
		
		public IList<CMSContentViewModel> Facilities { get; set; }

		public async Task OnGetAsync()
		{
			Facilities = (await _cmsRepository.GetCMSContent("Facility")).ToList();
		}
	}
}