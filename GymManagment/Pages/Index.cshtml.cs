﻿using GymManagment.Core;
using GymManagment.Core.ViewModels;
using GymManagment.Repository;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace GymManagment.Pages
{
    public class IndexModel : PageModel
    {
        private readonly ICMSRepository _cmsRepository;
        private readonly ApplicationDbContext _context;

        public IndexModel(ICMSRepository cmsRepository, ApplicationDbContext context)
        {
            _cmsRepository = cmsRepository;
            _context = context;
        }


        public IList<CMSContentViewModel> Facilities { get; set; }
        public CMSContentViewModel Section { get; set; }
        public IList<CMSContentViewModel> WorkoutPrograms { get; set; }
        public CMSContentViewModel AboutUs { get; set; }
        public IList<CMSContentViewModel> Testimonials { get; set; }
        public IList<BlogViewModel> Blogs { get; set; }
        public IList<CMSContentViewModel> Clients { get; set; }
        public IList<NewsViewModel> News { get; set; }

        public async Task OnGetAsync()
        {
            Section = (await _cmsRepository.GetCMSContent("Section")).ToList().FirstOrDefault();
            Facilities = (await _cmsRepository.GetCMSContent("Facility", 4)).ToList();
            WorkoutPrograms = (await _cmsRepository.GetCMSContent("WorkoutProgram", 4)).ToList();
            AboutUs = (await _cmsRepository.GetCMSContent("AboutUs")).ToList().FirstOrDefault();
            Testimonials = (await _cmsRepository.GetCMSContent("Testimonial")).ToList();
            Blogs = (await _cmsRepository.GetBlogs(3)).ToList();
            Clients = (await _cmsRepository.GetCMSContent("Client")).OrderBy(x => x.Text1).ToList();
            News = (await _cmsRepository.GetNews(3)).ToList();

            foreach (var item in Blogs)
            {
                item.Content = Regex.Replace(item.Content, @"(?></?\w+)(?>(?:[^>'""]+|'[^']*'|""[^""]*"")*)>", "").Trim();
                item.Content = item.Content.Substring(0, 100);
            }
        }
    }
}
