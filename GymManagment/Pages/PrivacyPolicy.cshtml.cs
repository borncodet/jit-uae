﻿using GymManagment.Core;
using GymManagment.Repository;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System.Threading.Tasks;

namespace GymManagment.Pages
{
    public class PrivacyPolicyModel : PageModel
    {
        private readonly ICMSRepository _cmsRepository;
        private readonly ApplicationDbContext _context;

        public PrivacyPolicyModel(ICMSRepository cmsRepository, ApplicationDbContext context)
        {
            _cmsRepository = cmsRepository;
            _context = context;
        }

        public async Task OnGetAsync()
        {
        }
    }
}
