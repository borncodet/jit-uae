﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GymManagment.Pages.Shared
{
	public static class ManageNavPages
	{
		public static string Index => "index";

		public static string AboutUs => "aboutus";

		public static string Program => "program";

		public static string Facilities => "facilities";

		public static string ContactUs => "contactus";

		public static string Blog => "blogs";

		public static string IndexNavClass(ViewContext viewContext) => PageNavClass(viewContext, Index);

		public static string AboutUsNavClass(ViewContext viewContext) => PageNavClass(viewContext, AboutUs);

		public static string ProgramNavClass(ViewContext viewContext) => PageNavClass(viewContext, Program);

		public static string FacilitiesNavClass(ViewContext viewContext) => PageNavClass(viewContext, Facilities);

		public static string ContactUsNavClass(ViewContext viewContext) => PageNavClass(viewContext, ContactUs);

		public static string BlogNavClass(ViewContext viewContext) => PageNavClass(viewContext, Blog);

		private static string PageNavClass(ViewContext viewContext, string page)
		{
			var activePage = viewContext.ViewData["ActivePage"] as string
				?? System.IO.Path.GetFileNameWithoutExtension(viewContext.ActionDescriptor.DisplayName);
			return string.Equals(activePage, page, StringComparison.OrdinalIgnoreCase) ? "active" : null;
		}
	}
}
