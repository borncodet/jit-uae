﻿using GymManagment.Core;
using GymManagment.Core.ViewModels;
using GymManagment.Repository;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GymManagment.Pages
{
    public class NewsModel : PageModel
    {
        private readonly ICMSRepository _cmsRepository;
        private readonly ApplicationDbContext _context;

        public IList<NewsViewModel> News { get; set; }
        public NewsModel(ICMSRepository cmsRepository, ApplicationDbContext context)
        {
            _cmsRepository = cmsRepository;
            _context = context;
        }

        public async Task OnGetAsync()
        {
            News = (await _cmsRepository.GetNews(3)).ToList();
        }
    }
}
