﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using GymManagment.Core.ViewModels;
using GymManagment.Repository;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace GymManagment.Pages
{
    public class BlogsModel : PageModel
    {
		private readonly ICMSRepository _cmsRepository;

		public BlogsModel(ICMSRepository cmsRepository)
		{
			_cmsRepository = cmsRepository;
		}

		public IList<BlogViewModel> Blogs { get; set; }
		public async Task OnGetAsync()
        {
			Blogs = (await _cmsRepository.GetBlogs()).ToList();

			foreach (var item in Blogs)
			{
				item.Content = Regex.Replace(item.Content, @"(?></?\w+)(?>(?:[^>'""]+|'[^']*'|""[^""]*"")*)>", "").Trim();
				item.Content = item.Content.Substring(0, 100);
			}
		}
    }
}