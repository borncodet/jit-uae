﻿using GymManagment.Core.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System.Threading.Tasks;

namespace GymManagment.Pages.Countries
{
	[Authorize]
	public class CreateModel : PageModel
	{
		private readonly GymManagment.Core.ApplicationDbContext _context;

		public CreateModel(GymManagment.Core.ApplicationDbContext context)
		{
			_context = context;
		}

		public IActionResult OnGet()
		{
			return Page();
		}

		[BindProperty]
		public Country Country { get; set; }


		public async Task<IActionResult>
			OnPostAsync()
		{
			if (!ModelState.IsValid)
			{
				return Page();
			}

			_context.Countries.Add(Country);
			await _context.SaveChangesAsync();

			return RedirectToPage("./Index");
		}
	}
}
