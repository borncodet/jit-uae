﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using GymManagment.Shared.Helpers;
		using GymManagment.Core;
		using GymManagment.Core.Models;
using Microsoft.AspNetCore.Authorization;

namespace GymManagment.Pages.Countries
{
	[Authorize]
	public class IndexModel : PageModel
	{
		private readonly GymManagment.Core.ApplicationDbContext _context;

		[BindProperty(SupportsGet = true)]
		public int CurrentPage { get; set; } = 1;

		[BindProperty(SupportsGet = true)]
		public int Count { get; set; } = 10;

		[BindProperty(SupportsGet = true)]
		public int TotalPages { get; set; }

		[BindProperty(SupportsGet = true)]
		public string SearchString { get; set; }

		public IndexModel(GymManagment.Core.ApplicationDbContext context)
		{
			_context = context;
		}

		public PagedListViewModel<Country> Country { get;set; }

		public async Task OnGetAsync()
		{
			var pagedList = new PagedList<Country>();
			var datas = from m in _context.Countries
						select m;
			if (!string.IsNullOrEmpty(SearchString))
			{
				datas = datas.Where(s => s.Name.Contains(SearchString));
			}

			Country= await pagedList.GetPagedListAsync(datas, CurrentPage, Count);
			CurrentPage = (Country).PageIndex;
			TotalPages = (Country).TotalPages;
		}
	}
}

