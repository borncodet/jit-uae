﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using GymManagment.Core;
using GymManagment.Core.Models;
using Microsoft.AspNetCore.Authorization;

namespace GymManagment.Pages.Countries
{
	[Authorize]
	public class DetailsModel : PageModel
    {
        private readonly GymManagment.Core.ApplicationDbContext _context;

        public DetailsModel(GymManagment.Core.ApplicationDbContext context)
        {
            _context = context;
        }

        public Country Country { get; set; }

        public async Task<IActionResult> OnGetAsync(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Country = await _context.Countries.FirstOrDefaultAsync(m => m.Id == id);

            if (Country == null)
            {
                return NotFound();
            }
            return Page();
        }
    }
}
