﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GymManagment.Core;
using GymManagment.Core.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;

namespace GymManagment.Pages
{
    public class SettingsModel : PageModel
    {
		private readonly ApplicationDbContext _context;

		public SettingsModel(ApplicationDbContext context)
		{
			_context = context;
		}

		[BindProperty]
		public GeneralSettings Settings { get; set; }

		public async Task OnGetAsync()
        {
			Settings= await _context.GeneralSettings.FirstOrDefaultAsync();
		}

		public async Task<IActionResult> OnPostAsync()
		{
			if (!ModelState.IsValid)
			{
				return Page();
			}

			if (string.IsNullOrEmpty(Settings.Id))
				_context.GeneralSettings.Add(Settings);
			else
				_context.Attach(Settings).State = EntityState.Modified;

			await _context.SaveChangesAsync();
			return RedirectToPage("./Home");
		}
	}
}