﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GymManagment.Core.ViewModels;
using GymManagment.Repository;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace GymManagment.Pages
{
    public class BlogModel : PageModel
    {
		private readonly ICMSRepository _cmsRepository;

		public BlogModel(ICMSRepository cmsRepository)
		{
			_cmsRepository = cmsRepository;
		}

		public BlogViewModel Data { get; set; }

		public async Task OnGetAsync(string url)
        {
			Data = await _cmsRepository.GetBlogByUrl(url);
		}
    }
}