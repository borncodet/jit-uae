﻿using GymManagment.Core;
using GymManagment.Core.ViewModels;
using GymManagment.Repository;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace GymManagment.Pages
{
    public class Index_01Model : PageModel
    {
        private readonly ICMSRepository _cmsRepository;
        private readonly ApplicationDbContext _context;

        public Index_01Model(ICMSRepository cmsRepository, ApplicationDbContext context)
        {
            _cmsRepository = cmsRepository;
            _context = context;
        }


        public IList<CMSContentViewModel> Facilities { get; set; }
        public IList<CMSContentViewModel> WorkoutPrograms { get; set; }
        public CMSContentViewModel AboutUs { get; set; }
        public IList<CMSContentViewModel> Testimonials { get; set; }
        public IList<BlogViewModel> Blogs { get; set; }

        public async Task OnGetAsync()
        {

            Facilities = (await _cmsRepository.GetCMSContent("Facility", 4)).ToList();
            WorkoutPrograms = (await _cmsRepository.GetCMSContent("WorkoutProgram", 4)).ToList();
            AboutUs = (await _cmsRepository.GetCMSContent("AboutUs")).ToList().FirstOrDefault();
            Testimonials = (await _cmsRepository.GetCMSContent("Testimonial")).ToList();
            Blogs = (await _cmsRepository.GetBlogs(3)).ToList();

            foreach (var item in Blogs)
            {
                item.Content = Regex.Replace(item.Content, @"(?></?\w+)(?>(?:[^>'""]+|'[^']*'|""[^""]*"")*)>", "").Trim();
                item.Content = item.Content.Substring(0, 100);
            }
        }
    }
}
