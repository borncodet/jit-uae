﻿using GymManagment.Core;
using GymManagment.Core.Models;
using GymManagment.Core.PostModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GymManagment.Pages.Admin
{
    public class BannerListModel : PageModel
    {
        private readonly ApplicationDbContext _context;

        public BannerListModel(ApplicationDbContext context)
        {
            _context = context;
        }

        public List<CMSContent> Items { get; set; }

        [BindProperty]
        public CMSContentViewPostModel Master { get; set; }

        public bool DeleteConfirmed = false;

        public Dictionary<string, string> CMSValue = new Dictionary<string, string>()
        {
            { "IndexBanner","Index" },
            { "ContactUsBanner","Contact Us" },
            { "ServiceBanner","Service" },
            { "HRMBanner","HRM Service" },
            { "MaritimeBanner","Maritime Service" },
            { "TechnicalBanner","Technical Service" },
            { "ConsultancyBanner","Consultancy Service" },
            { "OilBanner","Oil Spill Response Service" },
            { "DredgingBanner","Dredging Services" },
            { "ProjectBanner","Projects" },
            { "AboutUsMainBanner","About Us Main" },
            { "OpeningBanner","Current Openings" }
        };

        public async Task OnGetAsync()
        {
            Items = await _context.CMSContents.Where(i => i.CMSKey == "IndexBanner" ||
                                                     i.CMSKey == "ContactUsBanner" ||
                                                     i.CMSKey == "ServiceBanner" ||
                                                     i.CMSKey == "HRMBanner" ||
                                                     i.CMSKey == "MaritimeBanner" ||
                                                     i.CMSKey == "TechnicalBanner" ||
                                                     i.CMSKey == "ConsultancyBanner" ||
                                                     i.CMSKey == "OilBanner" ||
                                                     i.CMSKey == "DredgingBanner" ||
                                                     i.CMSKey == "ProjectBanner" ||
                                                     i.CMSKey == "AboutUsMainBanner" ||
                                                     i.CMSKey == "OpeningBanner").ToListAsync();
            DeleteConfirmed = false;
        }

        public async Task<IActionResult> OnPostDeleteAsync(string id)
        {
            if (!string.IsNullOrEmpty(id))
            {
                var data = await _context.CMSContents.Where(x => x.Id == id).FirstOrDefaultAsync();
                if (data != null)
                {
                    _context.CMSContents.Remove(data);
                    _context.SaveChanges();
                }
            }
            Items = await _context.CMSContents.Where(i => i.CMSKey == "IndexBanner" ||
                                                     i.CMSKey == "ContactUsBanner" ||
                                                     i.CMSKey == "HRMBanner" ||
                                                     i.CMSKey == "ServiceBanner" ||
                                                     i.CMSKey == "MaritimeBanner" ||
                                                     i.CMSKey == "TechnicalBanner" ||
                                                     i.CMSKey == "ConsultancyBanner" ||
                                                     i.CMSKey == "OilBanner" ||
                                                     i.CMSKey == "DredgingBanner" ||
                                                     i.CMSKey == "ProjectBanner" ||
                                                     i.CMSKey == "AboutUsMainBanner" ||
                                                     i.CMSKey == "OpeningBanner").ToListAsync();
            DeleteConfirmed = true;
            return Page();
        }
    }
}
