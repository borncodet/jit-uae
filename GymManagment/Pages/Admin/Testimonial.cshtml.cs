﻿using AutoMapper;
using GymManagment.Core;
using GymManagment.Core.Models;
using GymManagment.Core.PostModels;
using GymManagment.Repository.Interfaces;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace GymManagment.Pages.Admin
{
    public class TestimonialModel : BasePageModel
    {
        private readonly IMediaRepository _mediaRepository;
        private readonly IMapper _mapper;

        public TestimonialModel(ApplicationDbContext context,
            IHttpContextAccessor httpContextAccessor,
            IMediaRepository mediaRepository,
            IMapper mapper,
            IHostingEnvironment env) : base(context, httpContextAccessor, env)
        {
            _mediaRepository = mediaRepository;
            _mapper = mapper;
        }

        [BindProperty]
        public CMSContentViewPostModel Master { get; set; }

        public string ImageURL { get; set; }

        public async Task OnGetAsync(string id)
        {
            if (!string.IsNullOrEmpty(id))
            {
                var data = await _context.CMSContents.Include(s => s.Media).FirstOrDefaultAsync(m => m.Id == id);
                Master = _mapper.Map<CMSContentViewPostModel>(data);
                if (data.Media != null)
                {
                    ImageURL = data.Media.FileName;
                }
            }
        }


        public async Task<IActionResult> OnPostSaveAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            #region Save Media 
            if (Master.Image != null)
            {
                var mediaResult = await _mediaRepository.SaveMedia(CurrentUserId, Master.Image, "Testimonial");

                if (mediaResult.IsSuccess)
                {
                    Master.MediaId = mediaResult.MediaID;
                }

            }
            #endregion

            var data = _mapper.Map<CMSContentViewPostModel, CMSContent>(Master);
            if (string.IsNullOrEmpty(Master.Id))
            {
                _context.CMSContents.Add(data);
            }
            else
            {
                _context.Attach(data).State = EntityState.Modified;
            }

            await _context.SaveChangesAsync();

            return Redirect("TestimonialList");
        }
    }
}
