﻿using AutoMapper;
using GymManagment.Core;
using GymManagment.Core.Models;
using GymManagment.Repository.Interfaces;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace GymManagment.Pages.Admin
{
    public class SettingsModel : BasePageModel
    {
        private readonly IMediaRepository _mediaRepository;
        private readonly IMapper _mapper;

        public SettingsModel(ApplicationDbContext context,
             IHttpContextAccessor httpContextAccessor,
             IMediaRepository mediaRepository,
             IMapper mapper,
             IHostingEnvironment env) : base(context, httpContextAccessor, env)
        {
            _mediaRepository = mediaRepository;
            _mapper = mapper;
        }

        [BindProperty]
        public GeneralSettings Settings { get; set; }

        public async Task OnGetAsync()
        {
            Settings = await _context.GeneralSettings.FirstOrDefaultAsync();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            if (string.IsNullOrEmpty(Settings.Id))
            {
                _context.GeneralSettings.Add(Settings);
            }
            else
            {
                _context.Attach(Settings).State = EntityState.Modified;
            }

            await _context.SaveChangesAsync();
            return RedirectToPage("../Home");
        }
    }
}
