﻿using AutoMapper;
using GymManagment.Core;
using GymManagment.Core.Models;
using GymManagment.Repository.Interfaces;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace GymManagment.Pages.Admin
{
    public class OurValuesModel : BasePageModel
    {
        private readonly IMediaRepository _mediaRepository;
        private readonly IMapper _mapper;

        public OurValuesModel(ApplicationDbContext context,
            IHttpContextAccessor httpContextAccessor,
            IMediaRepository mediaRepository,
            IMapper mapper,
            IHostingEnvironment env) : base(context, httpContextAccessor, env)
        {
            _mediaRepository = mediaRepository;
            _mapper = mapper;
        }

        [BindProperty]
        public CMSContent Mission { get; set; }
        [BindProperty]
        public CMSContent Vision { get; set; }
        [BindProperty]
        public CMSContent CoreValue { get; set; }
        [BindProperty]
        public CMSContent Team { get; set; }

        public async Task OnGetAsync()
        {
            Mission = await _context.CMSContents.Where(c => c.CMSKey == "Mission").FirstOrDefaultAsync();
            Vision = await _context.CMSContents.Where(c => c.CMSKey == "Vision").FirstOrDefaultAsync();
            CoreValue = await _context.CMSContents.Where(c => c.CMSKey == "CoreValue").FirstOrDefaultAsync();
            Team = await _context.CMSContents.Where(c => c.CMSKey == "Team").FirstOrDefaultAsync();
        }


        public async Task<IActionResult> OnPostSaveAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }
            if (string.IsNullOrEmpty(Mission.Id))
            {
                Mission.CMSKey = "Mission";
                _context.CMSContents.Add(Mission);
            }
            else
            {
                _context.Attach(Mission).State = EntityState.Modified;
            }

            if (string.IsNullOrEmpty(Vision.Id))
            {
                Vision.CMSKey = "Vision";
                _context.CMSContents.Add(Vision);
            }
            else
            {
                _context.Attach(Vision).State = EntityState.Modified;
            }

            if (string.IsNullOrEmpty(CoreValue.Id))
            {
                CoreValue.CMSKey = "CoreValue";
                _context.CMSContents.Add(CoreValue);
            }
            else
            {
                _context.Attach(CoreValue).State = EntityState.Modified;
            }

            if (string.IsNullOrEmpty(Team.Id))
            {
                Team.CMSKey = "Team";
                _context.CMSContents.Add(Team);
            }
            else
            {
                _context.Attach(Team).State = EntityState.Modified;
            }

            await _context.SaveChangesAsync();
            return Redirect("../Home");
        }
    }
}
