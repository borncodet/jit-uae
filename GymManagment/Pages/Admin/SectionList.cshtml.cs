﻿using GymManagment.Core;
using GymManagment.Core.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GymManagment.Pages.Admin
{
    public class SectionListModel : PageModel
    {
        private readonly ApplicationDbContext _context;

        public SectionListModel(ApplicationDbContext context)
        {
            _context = context;
        }

        public List<CMSContent> Items { get; set; }

        public bool DeleteConfirmed = false;

        public async Task OnGetAsync()
        {
            Items = await _context.CMSContents.Where(i => i.CMSKey == "Section").OrderByDescending(x => x.CreatedOn).ToListAsync();
            DeleteConfirmed = false;
        }

        public async Task<IActionResult> OnPostDeleteAsync(string id)
        {
            if (!string.IsNullOrEmpty(id))
            {
                var data = await _context.CMSContents.Where(x => x.Id == id).FirstOrDefaultAsync();
                if (data != null)
                {
                    _context.CMSContents.Remove(data);
                    _context.SaveChanges();
                }
            }
            Items = await _context.CMSContents.Where(i => i.CMSKey == "Section").OrderByDescending(x => x.CreatedOn).ToListAsync();
            DeleteConfirmed = true;
            return Page();
        }
    }
}
