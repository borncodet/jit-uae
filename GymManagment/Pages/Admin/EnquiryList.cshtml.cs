﻿using GymManagment.Core;
using GymManagment.Core.Models;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GymManagment.Pages.Admin
{
    public class EnquiryListModel : PageModel
    {
        private readonly ApplicationDbContext _context;

        public EnquiryListModel(ApplicationDbContext context)
        {
            _context = context;
        }

        public List<Enquiry> Items { get; set; }

        public async Task OnGetAsync()
        {
            Items = await _context.Enquiries.OrderByDescending(x => x.CreatedOn).ToListAsync();
        }
    }
}
