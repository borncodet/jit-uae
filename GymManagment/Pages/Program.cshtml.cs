﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GymManagment.Core;
using GymManagment.Core.ViewModels;
using GymManagment.Repository;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace GymManagment.Pages
{
    public class ProgramModel : PageModel
    {
		private readonly ICMSRepository _cmsRepository;
		private readonly ApplicationDbContext _context;

		public ProgramModel(ICMSRepository cmsRepository, ApplicationDbContext context)
		{
			_cmsRepository = cmsRepository;
			_context = context;
		}
		
		public IList<CMSContentViewModel> WorkoutPrograms { get; set; }

		public async Task OnGetAsync()
		{
			WorkoutPrograms = (await _cmsRepository.GetCMSContent("WorkoutProgram")).ToList();
		}
	}
}