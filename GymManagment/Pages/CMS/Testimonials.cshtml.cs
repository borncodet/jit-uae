﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GymManagment.Core;
using GymManagment.Core.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;

namespace GymManagment.Pages.CMS
{
    public class TestimonialsModel : PageModel
    {
		private readonly ApplicationDbContext _context;

		public TestimonialsModel(ApplicationDbContext context)
		{
			_context = context;
		}

		public List<CMSContent> Items { get; set; }

		public async Task OnGetAsync()
        {
			Items = await _context.CMSContents.Where(i=>i.CMSKey== "Testimonial").ToListAsync();
		}
    }
}