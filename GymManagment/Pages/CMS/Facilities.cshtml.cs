﻿using GymManagment.Core;
using GymManagment.Core.Models;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GymManagment.Pages.CMS
{
    public class FacilitiesModel : PageModel
    {
        private readonly ApplicationDbContext _context;

        public FacilitiesModel(ApplicationDbContext context)
        {
            _context = context;
        }

        public List<CMSContent> Items { get; set; }

        public async Task OnGetAsync()
        {
            Items = await _context.CMSContents.Where(i => i.CMSKey == "Facility").ToListAsync();
        }
    }
}
