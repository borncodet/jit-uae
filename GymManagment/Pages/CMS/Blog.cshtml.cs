﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using GymManagment.Core;
using GymManagment.Core.Models;
using GymManagment.Core.PostModels;
using GymManagment.Repository.Interfaces;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;

namespace GymManagment.Pages.CMS
{
    public class BlogModel : BasePageModel
    {
		private readonly IMediaRepository _mediaRepository;
		private readonly IMapper _mapper;

		public BlogModel(ApplicationDbContext context,
			IHttpContextAccessor httpContextAccessor,
			IMediaRepository mediaRepository,
			IMapper mapper,
			IHostingEnvironment env):base(context,httpContextAccessor,env)
		{
			_mediaRepository = mediaRepository;
			_mapper = mapper;
		}

		[BindProperty]
		public CMSContentViewPostModel Master { get; set; }

		public string ImageURL { get; set; }

		public async Task OnGetAsync(string id)
        {
			if(!string.IsNullOrEmpty(id))
			{
				var data = await _context.CMSContents.Include(s => s.Media).FirstOrDefaultAsync(m => m.Id == id);
				Master = _mapper.Map<CMSContentViewPostModel>(data);
				if(data.Media !=null)
					ImageURL = data.Media.FileName;
			}
        }


		public async Task<IActionResult> OnPostSaveAsync()
		{

			if (!ModelState.IsValid)
			{
				return Page();
			}

			#region Save Media 
			if (Master.Image != null)
			{
				var mediaResult = await _mediaRepository.SaveMedia(CurrentUserId, Master.Image, "Blogs");

				if (mediaResult.IsSuccess)
				{
					Master.MediaId = mediaResult.MediaID;
				}

			}
			#endregion

			Master.Text2 = (Master.Text2.Replace(" ", "-")).ToLower();

			var blogdata = _context.CMSContents.Where(i => i.Text2 == Master.Text2).FirstOrDefault();
			if(blogdata!=null)
			{
				int i = 1;
				while(true)
				{
					var blogdata1 = _context.CMSContents.Where(b => b.Text2 == Master.Text2+i).FirstOrDefault();
					if (blogdata1 == null)
					{
						Master.Text2=Master.Text2 + i;
						break;
					}
					i++;
				}
			}

			var data = _mapper.Map<CMSContentViewPostModel, CMSContent>(Master);
			if (string.IsNullOrEmpty(Master.Id))
				_context.CMSContents.Add(data);
			else
				_context.Attach(data).State = EntityState.Modified;

			await _context.SaveChangesAsync();

			return Redirect("./Blogs");
		}
	}
}