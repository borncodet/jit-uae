﻿using GymManagment.Core;
using GymManagment.Core.ViewModels;
using GymManagment.Repository;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading.Tasks;

namespace GymManagment.ViewComponents
{
    public class OpeningBannerViewComponent : ViewComponent
    {
        private readonly ICMSRepository _cmsRepository;
        private readonly ApplicationDbContext _context;

        public OpeningBannerViewComponent(ICMSRepository cmsRepository, ApplicationDbContext context)
        {
            _cmsRepository = cmsRepository;
            _context = context;
        }

        public async Task<IViewComponentResult> InvokeAsync(string key)
        {
            CMSContentSocialViewModel model = new CMSContentSocialViewModel
            {
                CMSContentViewModel = (await _cmsRepository.GetCMSContent(key)).ToList(),
                SocialLinks = _context.GeneralSettings.FirstOrDefault()
            };
            return View(model);
        }
    }
}
