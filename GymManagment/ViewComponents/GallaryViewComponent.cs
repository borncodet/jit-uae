﻿using GymManagment.Core;
using GymManagment.Core.ViewModels;
using GymManagment.Repository;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GymManagment.ViewComponents
{
	public class GallaryViewComponent : ViewComponent
	{
		private readonly ICMSRepository _cmsRepository;

		public GallaryViewComponent(ICMSRepository cmsRepository)
		{
			_cmsRepository = cmsRepository;
		}

		public async Task<IViewComponentResult> InvokeAsync(int count=0)
		{
			var items = (await _cmsRepository.GetCMSContent("Gallary", count)).ToList();
			return View(items);
		}
	}
}
