﻿using GymManagment.Core;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace GymManagment.ViewComponents
{
    public class OurValuesViewComponent : ViewComponent
    {
        private readonly ApplicationDbContext _context;

        public OurValuesViewComponent(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            //var model = await _context.CMSContents.Where(i => i.CMSKey == "Mission" || i.CMSKey == "Vision" || i.CMSKey == "CoreValue" || i.CMSKey == "Team").ToListAsync();
            var model = await _context.CMSContents.Where(i => i.CMSKey == "Mission" || i.CMSKey == "Vision").ToListAsync();
            return View(model);
        }
    }
}
