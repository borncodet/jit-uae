﻿using GymManagment.Core;
using GymManagment.Core.ViewModels;
using GymManagment.Repository;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading.Tasks;

namespace GymManagment.ViewComponents
{
    public class AboutUsViewComponent : ViewComponent
    {
        private readonly ICMSRepository _cmsRepository;
        private readonly ApplicationDbContext _context;

        public AboutUsViewComponent(ICMSRepository cmsRepository, ApplicationDbContext context)
        {
            _cmsRepository = cmsRepository;
            _context = context;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            WebsiteFooterViewModel model = new WebsiteFooterViewModel()
            {
                AboutUs = _context.GeneralSettings.FirstOrDefault(),
                Blogs = (await _cmsRepository.GetBlogs(2)).ToList()
            };
            return View(model);
        }
    }
}
