﻿using GymManagment.Core;
using GymManagment.Core.ViewModels;
using GymManagment.Repository;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GymManagment.ViewComponents
{
	public class ContactUsViewComponent : ViewComponent
	{
		private readonly ApplicationDbContext _context;

		public ContactUsViewComponent(ApplicationDbContext context)
		{
			_context = context;
		}

		public async Task<IViewComponentResult> InvokeAsync()
		{
			var model = await _context.GeneralSettings.FirstOrDefaultAsync();
			return View(model);
		}
	}
}
