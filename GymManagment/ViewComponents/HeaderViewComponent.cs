﻿using GymManagment.Core;
using GymManagment.Repository;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace GymManagment.ViewComponents
{
    public class HeaderViewComponent : ViewComponent
    {
        private readonly ICMSRepository _cmsRepository;
        private readonly ApplicationDbContext _context;

        public HeaderViewComponent(ICMSRepository cmsRepository, ApplicationDbContext context)
        {
            _cmsRepository = cmsRepository;
            _context = context;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            return View();
        }
    }
}
