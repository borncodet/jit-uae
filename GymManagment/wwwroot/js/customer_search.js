var searchMembershipNo, searchName, searchPhoneNo, customerId;

$('#btnSearchMember').on('click', function () {
    searchMembershipNo = $("#txtSearchMembershipNo").val();
    searchName = $("#txtSearchName").val();
    searchPhoneNo = $("#txtSearchMobileNo").val();
    getSearchResult();
});

getSearchResult = function () {
    var obj = {
        MembershipNo: searchMembershipNo,
        Name: searchName,
        MobileNumber: searchPhoneNo
    };
    Post("customer/search-customers", obj, "loadSearchResult");
}

loadSearchResult = function (data) {
    if (data.length == 1) {
        $("#txtMembershipNo").val(data[0]["MembershipNo"]);
        $("#SearchCustomerModel").modal('hide');
		customerId = data[0]["Id"];
		loadCustomerDetails();
    }
    else {
        $("#SearchCustomerModel").modal('show');
        $("#txtSearchName").val($("#txtMembershipNo").val());
        $("#txtMembershipNo").val('');
        $("#txtFirstName").val('');
        $("#txtLastName").val('');
        $("#tblGrid tbody").empty();
        $.each(data, function () {
            var entry = this;
            $("#tblGrid tbody")
                .append('<tr onclick="setMemberNo(' + entry.MembershipNo + ')" />')
                .children('tr:last')
                .append('<td class="text-center">' + entry.MembershipNo + '</td>')
                .append('<td>' + entry.FirstName + '</td>')
                .append('<td>' + entry.LastName + '</td>')
				.append('<td>' + entry.MobileNumber + '</td>');
        });
    }
}

setMemberNo = function (memberNo) {
	$("#SearchCustomerModel").modal('hide');
	$("#txtMembershipNo").val(memberNo);
	$("#txtMembershipNo").focus();
}


