﻿

var loadPreviousNumberAPIName;
var loadPreviousEntryFunctionName;


$(document).on("change", "#txtLoadDate", function (event) {
	$("#btnEdit").click();
});

$(document).on("blur", "#txtLoadDate", function (event) {
	$("#btnEdit").click();
});


$("#btnEdit").click(function () {

	if ($("#txtLoadDate").val() == '') {
		$('#txtLoadDate').val(new Date().toDateInputValue());
	}
	Post(loadPreviousNumberAPIName, $("#txtLoadDate").val(), "loadPreviousNumbers");
});


loadPreviousNumbers = function (data) {
	fillCombo("cmbNos", "Id", "Value", data, "");
}

$(document).on("click", "#btnLoad", function (event) {
	if ($("#txtLoadDate").val() == "") {
		$("#txtLoadDate").focus();
		return;
	}
	if ($("#cmbNos").val() == null || $("#cmbNos").val() == '') {
		$("#cmbNos").focus();
		return;
	}
	$("#LoadPreviousEntryModel").modal('hide');
	window[loadPreviousEntryFunctionName]($("#cmbNos").val());
});