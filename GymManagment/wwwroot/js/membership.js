﻿var endDate, monthCount, expiresOn, startDate, hasPreviousBalance;
var membershipCategories = [], paymentModes = [], items = [];
var itemFee = 0, itemMonth, itemDay, isHourly, itemQuantity, itemNeedQuantity;
var slNo = 0, totalFee = 0;
var totalAmountReceived=0;

//#region Page Load

$(document).ready(function () {
	$('#txtDate').val(new Date().toDateInputValue());
	$('#txtStartDate').val(new Date().toDateInputValue());
	$("#divBalanceAmount").hide();
	Post("membership/membership-onload", {}, "formLoading");
	$("#txtMembershipNo").focus();
});

formLoading = function (data) {
	membershipCategories = data.Categories;
	paymentModes = data.PaymentModes;
	fillCombo("cmbMembershipCategory", "MembershipCategoryId", "MembershipCategoryName", data.Categories, "<-Select One->");
	fillPaymentGrid();
}

//#endregion

//#region Membership No

$("#txtMembershipNo").blur(function () {
	if ($("#txtMembershipNo").val() != "") {
		if ($.isNumeric($("#txtMembershipNo").val())) {
			searchMembershipNo = $("#txtMembershipNo").val();
		}
		else {
			searchMembershipNo = 0;
			searchName = $("#txtMembershipNo").val();
		}
		getSearchResult();
	}
	else {
		clear();
	}
});

loadCustomerDetails = function () {
	Post("membership/load-customer-details", customerId, "loadDetails");
}

loadDetails = function (data) {
	items = [];
	fillGrid();
	$("#txtMembershipNo").val(data.MembershipNo);
	$("#txtFirstName").val(data.FirstName + ' ' + data.LastName);
	//hasPreviousBalance = data.HaveBalance;
	//if (hasPreviousBalance) {
	//	errorMessage("Customer has previous balance to pay.Please clear the due before renewing the membership !!");
	//	return;
	//}
}

//#endregion

//#region Membership Category

$("#cmbMembershipCategory").change(function () {

	if ($("#txtMembershipNo").val() == '') {
		errorMessage("Please select a Member first");
		$("#txtMembershipNo").focus();
		return;
	}

	for (var i = 0; i < membershipCategories.length; i++) {
		var item = membershipCategories[i];
		isHourly = 0;
		if (item["MembershipCategoryId"] == $("#cmbMembershipCategory").val()) {
			itemFee = parseFloat(item["Fee"]);
			itemNeedQuantity = item["NeedQuantityField"];
			if (!item.NeedQuantityField) {
				$("#divQuantity").hide();
			}
			else {
				$("#divQuantity").show();
				$("#txtQuantity").val('1');

				if (item["Months"] == 0 && item["DayCount"] == 0) {
					isHourly = 1;
				}
			}
			$("#txtFee").val(itemFee);
			itemMonth = item.Months;
			itemDay = item.DayCount;
			break;
		}
	}

	var obj = {
		"CustomerId": customerId,
		"MembershipCategoryId": $("#cmbMembershipCategory").val()
	};

	Post("membership/get-customer-service-start-date", obj, "setStartDate");

});

setStartDate = function (data) {
	startDate = new Date(data);
	$("#txtStartDate").val(startDate.toDateInputValue());
	setEndDate();
};

$("#txtStartDate").blur(function () {
	if (new Date($("#txtStartDate").val()) < startDate) {
		$("#txtStartDate").val(startDate.toDateInputValue());
	}
	setEndDate();
});

$("#txtQuantity").change(function () {
	setEndDate();
});

setEndDate = function () {

	if (isHourly) {
		$("#divEndDate").hide();
		itemQuantity = parseFloat($('#txtQuantity').val());
		fee = itemFee * itemQuantity;
		endDate = new Date($('#txtStartDate').val());
		$('#txtEndDate').val(endDate.toDateInputValue());
	}
	else {
		$("#divEndDate").show();
		endDate = new Date($('#txtStartDate').val());
		if (!itemNeedQuantity) {
			endDate.setMonth(endDate.getMonth() + itemMonth);
			endDate.setDate(endDate.getDate() - 1);
			fee = itemFee;
		}
		else {
			itemQuantity = parseFloat($('#txtQuantity').val());
			if (itemMonth > 0) {
				endDate.setMonth(endDate.getMonth() + itemQuantity);
				endDate.setDate(endDate.getDate() - 1);
			}
			else {
				endDate.setDate(endDate.getDate() + itemQuantity);
			}
			fee = itemFee * itemQuantity;
		}
		$('#txtEndDate').val(endDate.toDateInputValue());
	}

	$("#txtFee").val(fee);
}

//#endregion

//#region Add Button

$("#btnAdd").click(function () {

	//#region validation
	//clearBorder();
	var cntr = [];
	cntr.push($("#cmbMembershipCategory"));
	if (itemNeedQuantity)
		cntr.push($("#txtQuantity"));
	cntr.push($("#txtStartDate"));
	if (!isHourly)
		cntr.push($("#txtEndDate"));
	if (!isModelStateValid(cntr))
		return;

	//#endregion

	var row = {};
	if (slNo == 0) {
		row.SlNo = items.length + 1;
	}
	else {
		for (var i = 0; i < items.length; i++) {
			if (items[i]["SlNo"] == slNo) {
				row = items[i];
				break;
			}
		}
	}
	row.MembershipCategoryId = $("#cmbMembershipCategory").val();
	row.MembershipCategoryName = $("#cmbMembershipCategory option:selected").text();
	row.Quantity = $("#txtQuantity").val();
	row.StartDate = $("#txtStartDate").val();
	if (!isHourly)
		row.EndDate = $("#txtEndDate").val();
	row.Fee = $("#txtFee").val();

	if (slNo == 0) {
		items.push(row);
	}
	clearStrip();
	fillGrid();
	$("#cmbMembershipCategory").focus();
});

//#endregion

//#region Grid Function

fillGrid = function () {

	$("#tblItem tbody").empty();
	totalFee = 0;
	$.each(items, function () {
		var entry = this;
		totalFee += parseFloat(entry.Fee);

		$("#tblItem tbody")
			.append('<tr />')
			.children('tr:last')
			.append('<td class="grid-button" data-toggle="tooltip" data-placement="bottom" title="Delete" onclick= "deleteEntry(' + entry.SlNo + ');"><div class="glyphicon glyphicon-trash"></div></td>')
			.append('<td class="text-center">' + entry.SlNo + '</td>')
			.append('<td>' + entry.MembershipCategoryName + '</td>')
			.append('<td class="text-center">' + entry.Quantity + '</td>')
			.append('<td class="text-center">' + entry.StartDate + '</td>')
			.append('<td class="text-center">' + entry.EndDate + '</td>')
			.append('<td class="text-right">' + entry.Fee + '</td>');
	});

	$("#tblItem tbody")
		.append('<tr />')
		.children('tr:last')
		.append('<td colspan="6" class="text-right text-bold">Total</td>')
		.append('<td class="text-right text-bold">' + totalFee.toFixed(2) + '</td>');


}

deleteEntry = function (slno) {
	if (confirm('Are you sure want to remove?')) {
		for (var i = 0; i < items.length; i++) {
			if (items[i]["SlNo"] == slno) {
				break;
			}
		}
		items.splice(i, 1);
		for (; i < items.length; i++) {
			items[i]["SlNo"] = i + 1;
		}
		clearStrip();
		fillGrid();
		$("#cmbService").focus();
	}
}



fillPaymentGrid = function () {

	//$("#tblPaymentItem tbody").empty();
	//var i = 0;
	//$.each(paymentModes, function () {
	//	var entry = this;
	//	totalFee += parseFloat(entry.Fee);

	//	$("#tblPaymentItem tbody")
	//		.append('<tr />')
	//		.children('tr:last')
	//		.append('<td>' + entry.Name + '</td>')
	//		.append('<td><input type="text" class="form-control text-right" id="txtPaymentAmount_' + i + '" onchange="calculateTotalReceived()"/></td>');
	//	i++;
	//});

	var paymentDivData = "";
	var i = 0;
	$.each(paymentModes, function () {
		var entry = this;
		paymentDivData +=
			`<div class="form-group">
						<label for="exampleInputEmail1">${entry.Name}</label>
						<input type="text" class="form-control text-right" id="txtPaymentAmount_${i}" 
						onchange="calculateTotalReceived()"/>
			</div>`;
		i++;
	});

	$("#paymentDiv").html(paymentDivData)
}

calculateTotalReceived = function () {
	totalAmountReceived = 0;
	var i = 0;
	$.each(paymentModes, function () {
		var entry = this;
		totalAmountReceived += getDecimalText("txtPaymentAmount_"+i);
		i++;
	});

	if (totalFee > totalAmountReceived) {
		$("#divBalanceAmount").show();
		$("#txtBalanceAmount").val(totalFee - totalAmountReceived);
	}
	else {
		$("#divBalanceAmount").hide();
	}

	$("#txtTotalReceived").val(totalAmountReceived);
}

//#endregion

//#region Save

$("#btnSave").click(function () {


	if (hasPreviousBalance) {
		errorMessage("Customer has previous balance to pay.Please clear the due before renewing the membership !!");
		return;
	}

	//#region validation

	cntr = [];
	if (totalFee > totalAmountReceived) {

		if (new Date($("#txtNextPaymentDate").val()) < new Date()) {
			errorMessage("Next Payment date should not be past date");
			$("#txtNextPaymentDate").focus();
			return;
		}

		cntr.push($("#txtNextPaymentDate"));
	}
	cntr.push($("#txtMembershipNo"));
	cntr.push($("#txtDate"));
	if (!isModelStateValid(cntr))
		return;


	if (items.length<=0) {
		errorMessage("No Items Found");
		$("#cmbMembershipCategory").focus();
		return;
	}

	//#endregion

	if (totalAmountReceived > totalFee) {
		errorMessage("Balance amount should not be negative");
		return;
	}
		
	if (hasPreviousBalance) {
		errorMessage("Customer have previous pending amount to pay");
		return;
	}

	var obj = {};
	obj.Master = {
		MembershipNo: $("#txtMembershipNo").val(),
		CustomerName: $("#txtFirstName").val(),
		CustomerId: customerId,
		BillingDate: $("#txtDate").val(),
		AmountPayable: $("#txtAmountPayable").val(),
		AmountPayable: totalFee,
		AmountPaid: totalAmountReceived
	};
	if (totalFee > totalAmountReceived) {
		obj.Master.NextPaymentDate = $("#txtNextPaymentDate").val();
	}
	else {
		obj.Master.NextPaymentDate = new Date();
	}
	obj.Items = items;

	var i = 0;
	$.each(paymentModes, function () {
		var entry = this;
		this.Amount = getDecimalText("txtPaymentAmount_" + i);
		i++;
	});
	obj.Payments = paymentModes;
	PostWithResponse("membership/save-membership", obj, "saved");
});

saved = function (data) {
	clear();
	successMessage("Saved successfully");
	$("#txtMembershipNo").focus();
}

clear = function () {
	$("#txtMembershipNo").val('');
	$("#txtFirstName").val('');
	clearStrip();
}

clearStrip = function () {
	slNo = 0;
	$("#cmbMembershipCategory").val(null);
	$("#txtQuantity").val('1');
	$("#txtStartDate").val('');
	$("#txtEndDate").val('');
	$("#txtFee").val('');
	$("#btnAdd").val('Add');

	itemFee = 0;
	itemMonth = 0;
	itemDay = 0;
	isHourly = 0;
	itemQuantity = 1;
	itemNeedQuantity = 0;

	$.each(paymentModes, function () {
		this.Amount = null;
	});

	$("#tblPaymentItem tbody").empty();
	fillPaymentGrid();
	$("#txtTotalReceived").val('');
	$("#txtBalanceAmount").val('');
	$("#txtNextPaymentDate").val('');
}

//#endregion