﻿


$(function () {
	
	$('#btnShowReport').on('click', function (evt) {
		PostForm(evt, '/api/report/general-ledger-report', '#formBasic', "showReport");
	});

	showReport = function (data) {
		$("#tblGrid tbody").empty();
		var entries = "";
		var previosDate="";
		$.each(data, function () {
			var entry = this;
			entries = entries +`<tr>`;
			//if (previosDate != entry.Date) {
			//	previosDate = entry.Date;
				entries = entries +
					`<td>${entry.Date}</td>`;
			//}
			//else {
			//	entries = entries + `<td></td>`;
			//}
			entries = entries + `
				<td>${entry.VoucherNo}</td>
				<td>${entry.Particulars}</td>
				<td class="text-right">${entry.Debit}</td>
				<td class="text-right">${entry.Credit}</td>
				<td class="text-right">${entry.Balance}</td>
			</tr>`;
		});

		if (data.length == 0) {
			entries = `<tr><td colspan='6'>No Entries Found</td></tr>`;
		}

		//$("#tblGrid tbody").append(entries);

		$('#tblGrid').bootstrapTable('destroy');
		$('#tblGrid').bootstrapTable({
			data: entries,
			escape: 'false'
		});
	}

	
});