﻿$(function () {
    //Abhilash C K
    //evt - event
    //dtId - datatable id
    //formId - form id
    //apiName - api name
    LoadAttendanceDatatable = function (evt, dtId, formId, apiName) {

        evt.preventDefault();

        // Get form
        var form = $(formId)[0];

        // Create an FormData object
        var param = new FormData(form);

        //ajax call to load datatable
        $.ajax({
            type: "POST",
            url: apiName,
            data: param,
            processData: false,
            contentType: false,
            dataType: "json",
            success: function (result, textStatus, jqXHR) {
                //clear datatable if datatable is not empty
                if ($(dtId).DataTable().data().any()) {
                    $(dtId).empty();
                }

                //Check the length of the result data from api
                if (result == undefined || result == null || result.length == 0) {
                    $(dtId).DataTable({
                        "oLanguage": {
                            "sEmptyTable": "No Data Found."
                        },
                        "destroy": true,
                        "bLengthChange": false
                    });
                }
                else {

                    //get property names from first object of an array of object
                    var keys = Object.keys(result[0]);

                    var jsonObj = [];
                    item = {};
                    item["data"] = 'id';
                    item["defaultContent"] = '';
                    item["sortable"] = false;
                    jsonObj.push(item);

                    var i = 0;
                    //iterate over the property names to create columns entry of datatable
                    keys.forEach(function (value) {

                        i += 1;
                        item = {}
                        item["data"] = value;
                        item["autoWidth"] = true;

                        jsonObj.push(item);

                        //if the column creation of datatable is completed
                        if (i == keys.length) {

                            var columns = JSON.stringify(jsonObj);

                            console.log(columns);

                            var table = $(dtId).DataTable({
                                "aaData": result,
                                "destroy": true,
                                "columns": jsonObj,
                                "order": [],
                                "lengthMenu": [[10, 25, 50, 100, 300, 500, -1], [10, 25, 50, 100, 300, 500, 'All']],
                                "columnDefs": [
                                    { orderable: false, targets: [0] },
                                    { type: 'html-string', targets: [8] }
                                ],
                                "dom": 'Blfrtip',
                                "buttons": [
                                    'excelHtml5'
                                ],
                                "rowCallback": function (row, data, index) {
                                    //for setting color based on attendance status
                                    var days = data.AttendanceStatus;
                                    var active = "Active";
                                    var modActive = "Moderately Active";
                                    var inActive = "Inactive";
                                    if (days == active) {
                                        $('td:eq(1)', row).html("<span class='attendance-active'>" + data.MembershipNo + "</span>");
                                        $('td:eq(2)', row).html("<span class='attendance-active'>" + data.CustomerName + "</span>");
                                        $('td:eq(3)', row).html("<span class='attendance-active'>" + data.DOB + "</span>");
                                        $('td:eq(4)', row).html("<span class='attendance-active'>" + data.Gender + "</span>");
                                        $('td:eq(5)', row).html("<span class='attendance-active'>" + data.Phone + "</span>");
                                        $('td:eq(6)', row).html("<span class='attendance-active'>" + data.MembershipCategoryName + "</span>");
                                        $('td:eq(7)', row).html("<span class='attendance-active'>" + data.Duration + "</span>");
                                        $('td:eq(8)', row).html("<span class='attendance-active'>" + data.StartDate + "</span>");
                                        $('td:eq(9)', row).html("<span class='attendance-active'>" + data.ExpiresOn + "</span>");
                                        $('td:eq(10)', row).html("<span class='attendance-active'>" + data.Fee + "</span>");
                                        $('td:eq(11)', row).html("<span class='attendance-active'>" + data.AttendanceStatus + "</span>");
                                    }
                                    else if (days == modActive) {
                                        $('td:eq(1)', row).html("<span class='attendance-mod-active'>" + data.MembershipNo + "</span>");
                                        $('td:eq(2)', row).html("<span class='attendance-mod-active'>" + data.CustomerName + "</span>");
                                        $('td:eq(3)', row).html("<span class='attendance-mod-active'>" + data.DOB + "</span>");
                                        $('td:eq(4)', row).html("<span class='attendance-mod-active'>" + data.Gender + "</span>");
                                        $('td:eq(5)', row).html("<span class='attendance-mod-active'>" + data.Phone + "</span>");
                                        $('td:eq(6)', row).html("<span class='attendance-mod-active'>" + data.MembershipCategoryName + "</span>");
                                        $('td:eq(7)', row).html("<span class='attendance-mod-active'>" + data.Duration + "</span>");
                                        $('td:eq(8)', row).html("<span class='attendance-mod-active'>" + data.StartDate + "</span>");
                                        $('td:eq(9)', row).html("<span class='attendance-mod-active'>" + data.ExpiresOn + "</span>");
                                        $('td:eq(10)', row).html("<span class='attendance-mod-active'>" + data.Fee + "</span>");
                                        $('td:eq(11)', row).html("<span class='attendance-mod-active'>" + data.AttendanceStatus + "</span>");
                                    }
                                    else {
                                        $('td:eq(1)', row).html("<span class='attendance-in-active'>" + data.MembershipNo + "</span>");
                                        $('td:eq(2)', row).html("<span class='attendance-in-active'>" + data.CustomerName + "</span>");
                                        $('td:eq(3)', row).html("<span class='attendance-in-active'>" + data.DOB + "</span>");
                                        $('td:eq(4)', row).html("<span class='attendance-in-active'>" + data.Gender + "</span>");
                                        $('td:eq(5)', row).html("<span class='attendance-in-active'>" + data.Phone + "</span>");
                                        $('td:eq(6)', row).html("<span class='attendance-in-active'>" + data.MembershipCategoryName + "</span>");
                                        $('td:eq(7)', row).html("<span class='attendance-in-active'>" + data.Duration + "</span>");
                                        $('td:eq(8)', row).html("<span class='attendance-in-active'>" + data.StartDate + "</span>");
                                        $('td:eq(9)', row).html("<span class='attendance-in-active'>" + data.ExpiresOn + "</span>");
                                        $('td:eq(10)', row).html("<span class='attendance-in-active'>" + data.Fee + "</span>");
                                        $('td:eq(11)', row).html("<span class='attendance-in-active'>" + data.AttendanceStatus + "</span>");
                                    }
                                }
                            });

                            table.on('order.dt search.dt', function () {
                                table.column(0, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
                                    cell.innerHTML = i + 1;
                                    table.cell(cell).invalidate('dom');
                                });
                            }).draw();

                            $('.dt-button.buttons-excel.buttons-html5').addClass('btn');
                            $('.dt-button.buttons-excel.buttons-html5').addClass('btn-primary');
                            $('.dt-button.buttons-excel.buttons-html5').text('Export');
                            $('.dt-buttons').css("float", "left");
                        }
                    })
                }

            },
            error: function (data, textStatus, jqXHR) {
                console.log("ajax error");
                console.log(data.responseText);
            }
        });
    }

    //evt - event
    //dtId - datatable id
    //formId - form id
    //apiName - api name
    LoadStaffAttendanceDatatable = function (evt, dtId, formId, apiName) {

        evt.preventDefault();

        // Get form
        var form = $(formId)[0];

        // Create an FormData object
        var param = new FormData(form);

        //ajax call to load datatable
        $.ajax({
            type: "POST",
            url: apiName,
            data: param,
            processData: false,
            contentType: false,
            dataType: "json",
            success: function (result, textStatus, jqXHR) {
                //clear datatable if datatable is not empty
                if ($(dtId).DataTable().data().any()) {
                    $(dtId).empty();
                }

                //Check the length of the result data from api
                if (result == undefined || result == null || result.length == 0) {
                    $(dtId).DataTable({
                        "oLanguage": {
                            "sEmptyTable": "No Data Found."
                        },
                        "destroy": true,
                        "bLengthChange": false
                    });
                }
                else {

                    //get property names from first object of an array of object
                    var keys = Object.keys(result[0]);

                    var jsonObj = [];
                    item = {};
                    item["data"] = 'id';
                    item["defaultContent"] = '';
                    item["sortable"] = false;
                    jsonObj.push(item);

                    var i = 0;
                    //iterate over the property names to create columns entry of datatable
                    keys.forEach(function (value) {

                        i += 1;
                        item = {}
                        item["data"] = value;
                        item["autoWidth"] = true;

                        jsonObj.push(item);

                        //if the column creation of datatable is completed
                        if (i == keys.length) {

                            var columns = JSON.stringify(jsonObj);

                            console.log(columns);

                            var table = $(dtId).DataTable({
                                "aaData": result,
                                "destroy": true,
                                "columns": jsonObj,
                                "order": [],
                                "lengthMenu": [[10, 25, 50, 100, 300, 500, -1], [10, 25, 50, 100, 300, 500, 'All']],
                                "columnDefs": [
                                    { orderable: false, targets: [0] },
                                ],
                                "dom": 'Blfrtip',
                                "buttons": [
                                    'excelHtml5'
                                ],
                                "rowCallback": function (row, data, index) {
                                    var days = data.AttendanceStatus;
                                    var active = "Active";
                                    var modActive = "Moderately Active";
                                    var inActive = "Inactive";
                                    if (days == active) {
                                        $('td:eq(1)', row).html("<span class='attendance-active'>" + data.StaffName + "</span>");
                                        $('td:eq(2)', row).html("<span class='attendance-active'>" + data.PhoneNumber + "</span>");
                                        $('td:eq(3)', row).html("<span class='attendance-active'>" + data.JoinDate + "</span>");
                                        $('td:eq(4)', row).html("<span class='attendance-active'>" + data.Email + "</span>");
                                        $('td:eq(5)', row).html("<span class='attendance-active'>" + data.UserTypeName + "</span>");
                                        $('td:eq(6)', row).html("<span class='attendance-active'>" + data.CreatedDate + "</span>");
                                        $('td:eq(7)', row).html("<span class='attendance-active'>" + data.LoginTime + "</span>");
                                        $('td:eq(8)', row).html("<span class='attendance-active'>" + data.LogoutTime + "</span>");
                                        $('td:eq(9)', row).html("<span class='attendance-active'>" + data.TotalHours + "</span>");
                                        $('td:eq(10)', row).html("<span class='attendance-active'>" + data.AttendanceStatus + "</span>");
                                    }
                                    else if (days == modActive) {
                                        $('td:eq(1)', row).html("<span class='attendance-mod-active'>" + data.StaffName + "</span>");
                                        $('td:eq(2)', row).html("<span class='attendance-mod-active'>" + data.PhoneNumber + "</span>");
                                        $('td:eq(3)', row).html("<span class='attendance-mod-active'>" + data.JoinDate + "</span>");
                                        $('td:eq(4)', row).html("<span class='attendance-mod-active'>" + data.Email + "</span>");
                                        $('td:eq(5)', row).html("<span class='attendance-mod-active'>" + data.UserTypeName + "</span>");
                                        $('td:eq(6)', row).html("<span class='attendance-mod-active'>" + data.CreatedDate + "</span>");
                                        $('td:eq(7)', row).html("<span class='attendance-mod-active'>" + data.LoginTime + "</span>");
                                        $('td:eq(8)', row).html("<span class='attendance-mod-active'>" + data.LogoutTime + "</span>");
                                        $('td:eq(9)', row).html("<span class='attendance-mod-active'>" + data.TotalHours + "</span>");
                                        $('td:eq(10)', row).html("<span class='attendance-mod-active'>" + data.AttendanceStatus + "</span>");
                                    }
                                    else {
                                        $('td:eq(1)', row).html("<span class='attendance-in-active'>" + data.StaffName + "</span>");
                                        $('td:eq(2)', row).html("<span class='attendance-in-active'>" + data.PhoneNumber + "</span>");
                                        $('td:eq(3)', row).html("<span class='attendance-in-active'>" + data.JoinDate + "</span>");
                                        $('td:eq(4)', row).html("<span class='attendance-in-active'>" + data.Email + "</span>");
                                        $('td:eq(5)', row).html("<span class='attendance-in-active'>" + data.UserTypeName + "</span>");
                                        $('td:eq(6)', row).html("<span class='attendance-in-active'>" + data.CreatedDate + "</span>");
                                        $('td:eq(7)', row).html("<span class='attendance-in-active'>" + data.LoginTime + "</span>");
                                        $('td:eq(8)', row).html("<span class='attendance-in-active'>" + data.LogoutTime + "</span>");
                                        $('td:eq(9)', row).html("<span class='attendance-in-active'>" + data.TotalHours + "</span>");
                                        $('td:eq(10)', row).html("<span class='attendance-in-active'>" + data.AttendanceStatus + "</span>");
                                    }
                                }
                            });

                            table.on('order.dt search.dt', function () {
                                table.column(0, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
                                    cell.innerHTML = i + 1;
                                    table.cell(cell).invalidate('dom');
                                });
                            }).draw();

                            $('.dt-button.buttons-excel.buttons-html5').addClass('btn');
                            $('.dt-button.buttons-excel.buttons-html5').addClass('btn-primary');
                            $('.dt-button.buttons-excel.buttons-html5').text('Export');
                            $('.dt-buttons').css("float", "left");
                        }
                    })
                }

            },
            error: function (data, textStatus, jqXHR) {
                console.log("ajax error");
                console.log(data.responseText);
            }
        });
    }
});
