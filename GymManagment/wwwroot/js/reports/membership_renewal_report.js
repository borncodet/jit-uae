﻿


$(function () {
	
	$('#btnShowReport').on('click', function (evt) {
		PostForm(evt, '/api/report/membership-renewal-report', '#formBasic', "showReport");
	});

	showReport = function (data) {
		$("#tblGrid tbody").empty();
		var entries = "";
		var previosMembershipRenewalId="";
		$.each(data, function () {
			var entry = this;
			entries = entries +`<tr>`;
			if (previosMembershipRenewalId != entry.MembershipRenewalId) {
				previosMembershipRenewalId = entry.MembershipRenewalId;
				entries = entries +
					`<td>${entry.BillingDate}</td>
					<td>${entry.MembershipNo}</td>
					<td>${entry.CustomerName}</td>`;
			}
			else {
				entries = entries + `<td colspan="3"></td>`;
			}
			entries = entries + `<td>${entry.MembershipCategoryName}</td>
				<td>${entry.Duration}</td>
				<td>${entry.StartDate}</td>
				<td>${entry.EndDate}</td>
				<td>${entry.Fee}</td>
			</tr>`;
		});

		if (data.length == 0) {
			entries = `<tr><td colspan='11'>No Sales Found</td></tr>`;
		}

		$("#tblGrid tbody").append(entries);
	}

});