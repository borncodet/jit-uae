﻿


$(function () {
	
	$('#btnShowReport').on('click', function (evt) {
		PostForm(evt, '/api/report/sales-report', '#formBasic', "showReport");
	});

	showReport = function (data) {
		$("#tblGrid tbody").empty();
		var entries = "";
		var previosDate="";
		$.each(data, function () {
			var entry = this;
			entries = entries +`<tr>`;
			if (previosDate != entry.Date) {
				previosDate = entry.Date;
				entries = entries +
					`<td>${entry.Date}</td>`;
			}
			else {
				entries = entries + `<td></td>`;
			}
			entries = entries + `
				<td>${entry.VoucherNo}</td>
				<td>${entry.MembershipNo}</td>
				<td>${entry.CustomerName}</td>
				<td>${entry.ReceivedAmount}</td>
			</tr>`;
		});

		if (data.length == 0) {
			entries = `<tr><td colspan='5'>No Items Found</td></tr>`;
		}

		$("#tblGrid tbody").append(entries);
	}

});