﻿


$(function () {
	
	$('#btnShowReport').on('click', function (evt) {
		PostForm(evt, '/api/report/membership-report', '#formBasic', "showReport");
	});

	showReport = function (data) {
		$("#tblGrid tbody").empty();
		var entries = "";
		var slNo = 0;
		var previosMembershipNo="";
		$.each(data, function () {
			var entry = this;
			entries = entries +`<tr>`;
			if (previosMembershipNo != entry.MembershipNo) {
				slNo++;
				previosMembershipNo = entry.MembershipNo;
				entries = entries +
					`<td>${slNo}</td>
					<td>${entry.MembershipNo}</td>
					<td>${entry.CustomerName}</td>
					<td>${entry.DOB}</td>
					<td>${entry.Gender}</td>
					<td>${entry.Phone}</td>`;
			}
			else {
				entries = entries + `<td colspan="6"></td>`;
			}
			entries = entries + `<td>${entry.MembershipCategoryName}</td>
				<td>${entry.Duration}</td>
				<td>${entry.StartDate}</td>
				<td>${entry.ExpiresOn}</td>
				<td>${entry.Fee}</td>
			</tr>`;
		});

		if (data.length == 0) {
			entries = `<tr><td colspan='11'>No Membership Found</td></tr>`;
		}

		$("#tblGrid tbody").append(entries);
	}

});