﻿


$(function () {
	
	$('#btnShowReport').on('click', function (evt) {
		PostForm(evt, '/api/report/pending-fee-report', '#formBasic', "showReport");
	});

	showReport = function (data) {
		$("#tblGrid tbody").empty();
		var entries = "";
		var previosDate="";
		$.each(data, function () {
			var entry = this;
			entries = entries +`<tr>`;
			if (previosDate != entry.DueDate) {
				previosDate = entry.DueDate;
				entries = entries +
					`<td>${entry.DueDate}</td>`;
			}
			else {
				entries = entries + `<td></td>`;
			}
			entries = entries + `
				<td>${entry.MembershipNo}</td>
				<td>${entry.CustomerName}</td>
				<td class="text-right">${entry.FeePayable}</td>
				<td class="text-right">${entry.FeePaid}</td>
				<td class="text-right">${entry.Balance}</td>
			</tr>`;
		});

		if (data.length == 0) {
			entries = `<tr><td colspan='5'>No Items Found</td></tr>`;
		}

		$("#tblGrid tbody").append(entries);
	}

});