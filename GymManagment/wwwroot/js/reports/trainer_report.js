﻿


$(function () {
	
	$('#btnShowReport').on('click', function (evt) {
		PostForm(evt, '/api/report/trainer-report', '#formBasic', "showReport");
	});

	showReport = function (data) {
		$("#tblGrid tbody").empty();
		var entries = "";
		var previosTrainerName="";
		$.each(data, function () {
			var entry = this;
			entries = entries +`<tr>`;
			if (previosTrainerName != entry.TrainerName) {
				previosTrainerName = entry.TrainerName;
				entries = entries +
					`<td>${entry.TrainerName}</td>`;
			}
			else {
				entries = entries + `<td></td>`;
			}
			entries = entries + `
				<td>${entry.MembershipNo}</td>
				<td>${entry.CustomerName}</td>
				<td>${entry.Fee}</td>
				<td>${entry.StartDate}</td>
				<td>${entry.EndDate}</td>
			</tr>`;
		});

		if (data.length == 0) {
			entries = `<tr><td colspan='6'>No Members Found</td></tr>`;
		}

		$("#tblGrid tbody").append(entries);
	}

});