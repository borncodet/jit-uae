var customerId="", mediaId = "";

$(function () {

	//#region Page Load

	$(document).ready(function () {

		customerId = getIdFromURL();
		Post("customer/load-edit-customer",  customerId , "formLoading");

	});

	formLoading = function (data) {
		fillDefaultValues(data.DefaultValues);
		fillCustomerDetails(data.BasicDetails);
		fillPaymentSummary(data.PaymentSummary);
		fillPaymentHistory(data.PaymentHistory);
		fillWorkoutHistory(data.WorkoutHistories);
		fillSuggestions(data.DefaultValues);
	}

	fillDefaultValues = function (data) {
		fillCombo("cmbGender", "Id", "Value", data.Genders,"<-Select One->");
		fillCombo("cmbMaritalStatus", "Id", "Value", data.MaritalStatuses, "<-Select One->");
		fillCombo("cmbNamePrefix", "Id", "Value", data.NamePrefixes, "<-Select One->");
		fillCombo("cmbState", "Id", "Value", data.States, "<-Select One->");
		fillCombo("cmbCategory", "Id", "Value", data.MembershipCategories, "<-Select One->");
	}

	fillSuggestions = function (data) {
		fillSuggestion("AreaList", data.Areas);
		fillSuggestion("CityList", data.Cities);
		fillSuggestion("OccupationList", data.Occupations);
	}

	fillCustomerDetails = function (data) {
		//$('#txtDate').val(new Date(data.Date).toDateInputValue());
		$("#img_here").show();
		if (data.FileName != "" && data.FileName != null) {
			$("#img_here").attr('src', data.FileName);
		}
		else {
			$("#img_here").attr('src', "/theme/images/profile_img.png");
		}
		$("#cmbNamePrefix").val(data.NamePrefixId);
		$("#txtFirstName").val(data.FirstName);
		$("#txtLastName").val(data.LastName);
		$("#cmbGender").val(data.GenderId);
		$("#cmbMaritalStatus").val(data.MaritalStatusId);
		$("#txtOccupation").val(data.Occupation);
		$("#txtHouseName").val(data.HouseName);
		$("#txtArea").val(data.Area);
		$("#txtCity").val(data.City);
		$("#cmbState").val(data.StateId);
		$("#txtDOB").val(new Date(data.DOB).toDateInputValue());
		$("#txtPhone1").val(data.Phone);
		$("#txtPhone2").val(data.Phone2);
		$("#txtEmail").val(data.EmailId);
		$("#txtCompanyEmail").val(data.CompanyEmailId);
		$("#txtMembershipNo").val(data.MembershipNo);
		$("#txtRegisterDate").val(new Date(data.RegisteredDate).toDateInputValue());
		$("#txtExpiresOn").val(new Date(data.ExpiresOn).toDateInputValue());
		$("#cmbCategory").val(data.MembershipCategoryId);
	}

	fillPaymentSummary = function (data) {
		$("#spanPaid").text(data.Received);
		$("#spanBalance").text(data.Balance);
		$("#spanTotal").text(data.Total);

		if (data.Balance > 0) {
			$("#btnMembershipLink").hide();
			$("#btnPendingPaymentLink").show();
		}
		else {
			$("#btnMembershipLink").show();
			$("#btnPendingPaymentLink").hide();
		}
	}

	fillPaymentHistory = function (data) {
		$("#tblGridPaymentHistory tbody").empty();
		$.each(data, function () {
			var entry = this;
			$("#tblGridPaymentHistory tbody")
				.append('<tr/>')
				.children('tr:last')
				.append('<td class="text-center">' + new Date(entry.BillingDate).toDateInputValue()  + '</td>')
				.append('<td>' + entry.Description + '</td>')
				.append('<td>' + entry.Debit + '</td>')
				.append('<td>' + entry.Credit + '</td>')
				.append('<td class="text-right">' + entry.Balance + '</td>');
		});
	}

	fillWorkoutHistory = function (data) {
		$("#tblGridWorkoutHistory tbody").empty();
		$.each(data, function () {
			var entry = this;
			$("#tblGridWorkoutHistory tbody")
				.append('<tr/>')
				.children('tr:last')
				.append('<td style="width: 90px;">' + new Date(entry.FromDate).toDateInputValue() + '</td>')
				.append('<td style="width: 90px;">' + new Date(entry.ToDate).toDateInputValue() + '</td>')
				.append('<td>' + entry.Goal + '</td>')
				.append('<td>' + entry.EnteredBy + '</td>')
				.append('<td class="text-center"><a href="/WorkoutPlan?id=' + entry.Id + '"><i class= "fa fa-pencil-square-o" aria - hidden="true"></i></a></td>')
				.append('<td class="text-center"><a target="_blank" href="/WorkoutPlan/WorkoutChart?id=' + entry.Id + '"><i class= "fa fa-download" aria - hidden="true"></i></a></td>');
		});
	}

	//fa fa-pencil-square-o

	//#endregion

	//#region Profile Image
	readURL = function (input) {
		if (input.files && input.files[0]) {
			$("#img_here").show();
			
			var reader = new FileReader();
			reader.onload = function (e) {
				$("#img_here").attr('src', e.target.result);
			};
			reader.readAsDataURL(input.files[0]);
		}
		else {
			$("#img_here").hide();
		}
	}
	//#endregion

	//#region Save

	$("#btnSave").click(function () {

		//#region validation
		cntr = [];
		cntr.push($("#cmbNamePrefix"));
		cntr.push($("#txtFirstName"));
		cntr.push($("#cmbGender"));
		cntr.push($("#txtDOB"));
		cntr.push($("#txtPhone1"));
		cntr.push($("#txtEmail"));
		if (!isModelStateValid(cntr))
			return;

		//#endregion

		if (document.getElementById("uploadBtn").files.length > 0) {
			if (mediaId == "") {
				uploadImage("uploadBtn", 1, 'imageUploaded');
			}
		}
		else {
			mediaId = "";
			save();
		}
	});

	imageUploaded = function (data) {
		mediaId = data.Result.MediaId;
		save();
	}

	save = function () {
		var obj = {
			Id: customerId,
			NamePrefixId: $("#cmbNamePrefix").val(),
			FirstName: $("#txtFirstName").val(),
			LastName: $("#txtLastName").val(),
			GenderId: $("#cmbGender").val(),
			DOB: $("#txtDOB").val(),
			MaritalStatusId: $("#cmbMaritalStatus").val(),
			Occupation: $("#txtOccupation").val(),
			HouseName: $("#txtHouseName").val(),
			Area: $("#txtArea").val(),
			City: $("#txtCity").val(),
			StateId: $("#cmbState").val(),
			Phone: $("#txtPhone1").val(),
			Phone2: $("#txtPhone2").val(),
			EmailId: $("#txtEmail").val(),
			CompanyEmailId: $("#txtCompanyEmail").val(),
			MediaId: mediaId
		};
		PostWithResponse("customer/update", obj, "updated");
	}

	updated = function () {
		successMessage("Changes saved successfully");
	}

	//#endregion

	//#region Updates Status

	$("#btnChangeStatus").click(function () {

		var obj = {
			CustomerId: customerId
		};
		if ($("#btnChangeStatus").text() == "Inactive") {
			obj.IsActive = false;
		}
		else {
			obj.IsActive = true;
		}
		PostWithResponse("customer/update-status", obj, "statusUpdated");
	});
	
	statusUpdated = function () {
		successMessage("Status Updated successfully");
		if ($("#btnChangeStatus").text() == "Inactive") {
			$("#btnChangeStatus").text('Active');
		}
		else {
			$("#btnChangeStatus").text('Deactive');
		}
	}

	//#endregion

});