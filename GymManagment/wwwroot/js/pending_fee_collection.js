var membershipCategories = [];
var membershipRenewalId;

//#region Page Load

$(document).ready(function () {
	$('#txtDate').val(new Date().toDateInputValue());
	$('#txtStartDate').val(new Date().toDateInputValue());
	$("#divBalanceAmount").hide();
	Post("membership/membership-onload", {}, "formLoading");
});

formLoading = function (data) {
	membershipCategories = data.Categories;
	fillCombo("cmbMembershipCategory", "Id", "MembershipCategoryName", data.Categories, "<-Select One->");
}

//#endregion

loadCustomerDetails = function () {
	Post("membership/get-customer-fee-pending-details", customerId, "loadDetails");
}

loadDetails = function (data) {
	$("#txtFirstName").val(data.FirstName + ' ' + data.LastName);
	$("#txtExpiryDate").val(new Date(data.ExpiresOn).toDateInputValue());
	$("#txtAmountPayable").val(data.AmountReceivable);
	$("#cmbMembershipCategory").val(data.MembershipCategoryId);
	fee = data.AmountReceivable;
	membershipRenewalId = data.MembershipRenewalId;
}


$("#txtMembershipNo").blur(function () {
	if ($("#txtMembershipNo").val() != "") {
		if ($.isNumeric($("#txtMembershipNo").val())) {
			searchMembershipNo = $("#txtMembershipNo").val();
		}
		else {
			searchMembershipNo = 0;
			searchName = $("#txtMembershipNo").val();
		}
		getSearchResult();
	}
	else {
		clear();
	}
});



$("#txtCashAmount, #txtCardAmount").blur(function () {

	if (checkPayableAmountIsValid) {
		if (getDecimalText("txtCashAmount") + getDecimalText("txtCardAmount") < fee) {
			$("#divBalanceAmount").show();
			$("#txtBalanceAmount").val(fee - getDecimalText("txtCashAmount") - getDecimalText("txtCardAmount"));
		}
		else {
			$("#divBalanceAmount").hide();
		}
	}

});

checkPayableAmountIsValid = function () {
	if (getDecimalText("txtCashAmount") + getDecimalText("txtCardAmount") > fee) {
		errorMessage("Amount should not be greater than payable amount");
		$("#txtCashAmount").addClass('required-field');
		$("#txtCashAmount").focus();
		return false;
	}
	else
		return true;
}


//#region Save

$("#btnSave").click(function () {

	//#region validation

	cntr = [];
	if (getDecimalText("txtCashAmount") + getDecimalText("txtCardAmount") < fee) {

		if (new Date($("#txtNextPaymentDate").val()) < new Date()) {
			errorMessage("Next Payment date should not be past date");
			$("#txtNextPaymentDate").focus();
			return;
		}

		cntr.push($("#txtNextPaymentDate"));
	}
	cntr.push($("#txtMembershipNo"));
	cntr.push($("#txtDate"));
	if (getDecimalText("txtCardAmount")==0)
		cntr.push($("#txtCashAmount"));
	if (getDecimalText("txtCashAmount")==0)
		cntr.push($("#txtCardAmount"));	
	if (!isModelStateValid(cntr))
		return;

	//#endregion

	if (!checkPayableAmountIsValid())
		return;

	var obj = {
		CustomerId: customerId,
		BillingDate: $("#txtDate").val(),
		CashAmount: getDecimalText("txtCashAmount"),
		CardAmount: getDecimalText("txtCardAmount"),
		CustomerName: $("#txtFirstName").val(),
		MembershipNo: $("#txtMembershipNo").val(),
		MembershipRenewalId: membershipRenewalId,
		MembershipCategoryId: $("#cmbMembershipCategory").val()
	};
	if (getDecimalText("txtCashAmount") + getDecimalText("txtCardAmount") < fee) {
		obj.NextPaymentDate = $("#txtNextPaymentDate").val();
	}
	else {
		obj.NextPaymentDate = new Date();
	}
	PostWithResponse("membership/save-pending-fee-payment", obj, "saved");
});

saved = function (data) {
	clear();
	successMessage("Saved successfully");
	$("#txtMembershipNo").focus();
}

clear = function () {
	$("#txtMembershipNo").val('');
	$("#txtFirstName").val('');
	$("#txtExpiryDate").val('');
	$("#txtStartDate").val(new Date().toDateInputValue());
	$("#txtEndDate").val('');
	$("#txtAmountPayable").val('');
	$("#txtCashAmount").val('');
	$("#txtCardAmount").val('');
	$("#txtBalanceAmount").val('');
	$("#txtNextPaymentDate").val('');
	$("#divBalanceAmount").hide();
	$("#cmbMembershipCategory").val(null);
}

//#endregion