﻿$(function () {

	PostForm = function (evt, apiName, formId, successFun, msgDivId, msgId) {

		evt.preventDefault();
		var form = $(formId)[0];
		var formData = new FormData(form);

		//if (!$(formId).valid()) return false;
		$.ajax({
			url: apiName,
			processData: false,
			contentType: false,
			data: formData,
			type: 'POST'
		}).done(function (result) {
			window[successFun](result);
		}).fail(function (a, b, c) {
			errorMessage("Error Occured", msgDivId, msgId);
		});
	}

	$('#btnSaveEnquiry').click(function (evt) {
		PostForm(evt, '/Enquiry?handler=Save', '#formBasic', "saved");
	});

	saved = function () {
		$('#formBasic').each(function () {
			this.reset();
		});
	}
});