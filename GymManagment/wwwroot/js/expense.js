﻿var mediaId="";

//#region Page Load

$(document).ready(function () {
	$('#txtDate').val(new Date().toDateInputValue());
	$('#img_here').hide();
	Post("expense/onload", {}, "formLoading");
});

formLoading = function (data) {
	fillSuggestion("particulars_list", data.Particulars);
	$("#txtVoucherNo").val(data.VoucherNo);
	$('#cmbVoucherType').focus();
}

//#endregion

//#region VoucherType Changed

$("#cmbVoucherType").change(function () {
	Post("expense/change-voucher-type", $("#cmbVoucherType").val(), "voucherTypeChanged");
});

voucherTypeChanged = function (data) {
	$("#txtVoucherNo").val(data);
}

//#endregion

//#region Button Save
$("#btnSave").click(function () {

	//#region validation
	cntr = [];
	cntr.push($("#txtDate"));
	cntr.push($("#txtVoucherNo"));
	cntr.push($("#txtParticular"));
	cntr.push($("#txtAmount"));
	if (!isModelStateValid(cntr))
		return;

	//#endregion

	if (document.getElementById("img").files.length > 0) {
		if (mediaId == "") {
			uploadImage("img", 2,'imageUploaded');
		}
	}
	else {
		mediaId = "";
		save();
	}
});

imageUploaded = function (data) {
	mediaId = data.Result.MediaId;
	save();
}

save = function () {
	var obj = {
		Date: $("#txtDate").val(),
		VoucherTypeID: $("#cmbVoucherType").val(),
		VoucherNo: $("#txtVoucherNo").val(),
		Remarks: $("#txtRemarks").val(),
		Narration: $("#txtNarration").val(),
		Particulars: $("#txtParticular").val(),
		Amount: $("#txtAmount").val(),
		MediaId: mediaId
	};
	PostWithResponse("expense/save", obj, "saved");
}

saved = function (data) {
	clear();
	$("#txtVoucherNo").val(data.Result.VoucherNo);
	successMessage("Transaction Saved Successfully!!");
}
//#endregion

//#region Clear

clear = function (data) {
	$("#txtVoucherNo").val('');
	$("#txtRemarks").val('');
	$("#txtNarration").val('');
	$("#txtAmount").val('');
	$("#txtParticular").val('');
	$('#btnEdit').show();
	$("#btnNew").val('New');
	$('#btnSave').show();
	$("#img").val('');
	$("#img_here").hide();
	mediaId = "";
	$('#cmbVoucherType').focus();
}

//#endregion


//#region Select

select=function (journalMasterId) {
	Post("expense/select", journalMasterId, "loadEntry");
};

loadEntry = function (data) {
	$('#txtDate').val(new Date(data.Date).toDateInputValue());
	$("#cmbVoucherType").val(data.VoucherTypeId);
	$("#txtVoucherNo").val(data.VoucherNo);
	$("#txtRemarks").val(data.Remarks);
	$("#txtNarration").val(data.Narration);
	$("#txtParticular").val(data.Particulars);
	$("#txtAmount").val(data.Amount);
	if (data.FileName != "") {
		$("#img_here").show();
		$("#img_here").attr('src', data.FileName);
	}
	$('#btnSave').hide();
	$("#btnNew").val('Cancel');
}

//#endregion

//#region Button New
$("#btnNew").click(function () {
	clear();
	$("#cmbVoucherType").change();
});
//#endregion