Date.prototype.toDateInputValue = (function () {
	var local = new Date(this);
	local.setMinutes(this.getMinutes() - this.getTimezoneOffset());
	return local.toJSON().slice(0, 10);
});

fillCombo = function (name, valuefield, textfield, data, defaultitem, neednew) {
	var cmb = $("#" + name);
	cmb.empty();
	if (defaultitem == '<-Select One->' || defaultitem == '<-All->') {
		cmb.append($("<option/>").val(null).html(defaultitem));
	}
	else if (defaultitem != '') {
		cmb.append($("<option/>").val(0).html(defaultitem));
	}

	if (data != null) {
		$.each(data, function () {
			cmb.append($("<option/>").val(this[valuefield]).html(this[textfield]));
		});
	}
	if (neednew) {
		cmb.append($("<option/>").val(-1).html("<- New ->"));
	}
}

isModelStateValid = function (controls) {
	var missing = -1;
	for (i = 0; i < controls.length; i++) {
		if (controls[i].val() == '' || controls[i].val() == undefined) {
			controls[i].addClass('required-field');

			if (missing == -1)
				missing = i;
		}
		else {
			controls[i].removeClass('required-field');
		}
	}
	if (missing != -1) {
		cntr[missing].focus();
		return false;
	}
	return true;
}


getDecimalText = function (textboxName) {
	var value = 0;
	if ($("#" + textboxName).val() != "" && $("#" + textboxName).val() != undefined)
		value = parseFloat($("#" + textboxName).val());

	return value;
}

errorMessage = function (msg) {
	$('#msg').html(msg);
	$('#div_message').removeClass().addClass('alert alert-danger').show().delay(10000).hide("slow");
}

successMessage = function (msg) {
	$('#msg').html(msg);
	$('#div_message').removeClass().addClass('alert alert-success').show().delay(10000).hide("slow");
}


fillSuggestion = function (name, data) {
	var cmb = $("#" + name);
	cmb.empty();
	for (var i = 0; i < data.length; i++) {
		cmb.append($('<option/>').val(data[i]).html(data[i]));
	}
}


//#region Grid Generic Functions

getRow = function (slNo, dataList) {
	var entry = {};
	if (slNo == 0) {
		entry.SlNo = dataList.length + 1;
	}
	else {
		for (var i = 0; i < dataList.length; i++) {
			if (dataList[i]["SlNo"] == slNo) {
				entry = dataList[i];
				break;
			}
		}
	}
	return entry;
}

deleteRow = function (slNo, dataList) {
	for (var i = 0; i < dataList.length; i++) {
		if (dataList[i]["SlNo"] == slNo) {
			break;
		}
	}
	dataList.splice(i, 1);
	for (; i < dataList.length; i++) {
		dataList[i]["SlNo"] = i + 1;
	}
}

//#endregion


getIdFromURL = function() {
	var sPageURL = window.location.href;
	var indexOfLastSlash = sPageURL.lastIndexOf("id=");
	if (indexOfLastSlash > 0 && sPageURL.length - 1 != indexOfLastSlash)
		return sPageURL.substring(indexOfLastSlash + 3);
	else
		return "";
}


function isDecimal(evt, element) {
	var charcode = evt.which ? evt.which : event.KeyCode;
	if (charcode == 8 || (charcode == 46 && $(element).val().indexOf('.') == -1) || event.keyCode == 37 || event.keyCode == 39) {
		return true;
	}
	else if (charcode < 48 || charcode > 57) {
		return false;
	}
	else return true;
}

function isInteger(evt, element) {
	var charcode = evt.which ? evt.which : event.KeyCode;
	if (charcode == 8 || event.keyCode == 37 || event.keyCode == 39) {
		return true;
	}
	else if (charcode < 48 || charcode > 57) {
		return false;
	}
	else return true;
}

function isNegativeNumber(evt, element) {
	var charcode = evt.which ? evt.which : event.KeyCode;
	if (charcode == 8 || (charcode == 45 && $(element).val().indexOf('-') == -1) || (charcode == 46 && $(element).val().indexOf('.') == -1) || event.keyCode == 37 || event.keyCode == 39) {
		return true;
	}
	else if (charcode < 48 || charcode > 57) {
		return false;
	}
	else return true;
}

$(document).on("keypress", ".decimal", function (event) {
	return isNumber(event, this);
});


$(document).on("keypress", ".integer", function (event) {
	return isInteger(event, this);
});

$(document).on("keypress", ".negativenumber", function (event) {
	return isNegativeNumber(event, this);
});


//#region Image Upload
readURL = function (input,imageDiv) {
	if (input.files && input.files[0]) {
		$("#" + imageDiv).show();
		var reader = new FileReader();
		reader.onload = function (e) {
			$("#" + imageDiv).attr('src', e.target.result);
		};
		reader.readAsDataURL(input.files[0]);
	}
	else {
		$("#" + imageDiv).hide();
	}
}
//#endregion
 