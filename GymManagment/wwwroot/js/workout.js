﻿var masterId="";
var warmups=[], strengths=[], cardios=[], cooldowns=[];
var warmupSlNo = 0, strengthSlNo = 0, cardioSlNo = 0, cooldownSlNo = 0;

var gridName;

$(function () {

	//#region Page Load

	$(document).ready(function () {
		masterId = getIdFromURL();
		Post("workout/onload", masterId, "formLoading");
	});

	formLoading = function (data) {
		loadCombos(data);
	}

	loadCombos = function (data) {
		fillSuggestion("activitylist", data.ActivityList);
		fillSuggestion("excerciselist", data.ExcercisList);
		fillSuggestion("timelist", data.TimeList);

		fillCombo("cmbWarmupIntensity", "Id", "Value", data.IntensityList, "");
		fillCombo("cmbCardioIntensity", "Id", "Value", data.IntensityList, "");
		fillCombo("cmbCoolDownIntensity", "Id", "Value", data.IntensityList, "");

		if (masterId != "") {
			fillMasterData(data.Master);
			warmups = data.Warmups;
			listWarmUp();
			strengths = data.Strengths;
			listStrength();
			cardios = data.Cardios;
			listCardio();
			cooldowns = data.Cooldowns;
			listCooldown();
		}
	}

	//#endregion

	//#region Customer
	$("#txtMembershipNo").blur(function () {
		if ($("#txtMembershipNo").val() != "") {
			if ($.isNumeric($("#txtMembershipNo").val())) {
				searchMembershipNo = $("#txtMembershipNo").val();
			}
			else {
				searchMembershipNo = 0;
				searchName = $("#txtMembershipNo").val();
			}
			getSearchResult();
		}
		else {
			clear();
		}
	});

	loadCustomerDetails = function () {
		Post("membership/get-customer-name", customerId, "loadCustDetails");
	}

	loadCustDetails = function (data) {
		$("#txtFirstName").val(data.FirstName + ' ' + data.LastName);
	}
	//#endregion

	//#region Warmup Section

	$("#btnWarmupAdd").click(function () {

		//#region validation

		cntr = [];
		cntr.push($("#txtWarmupDayFrom"));
		cntr.push($("#txtWarmupActivity"));
		cntr.push($("#txtWarmupTimeDist"));
		cntr.push($("#txtWarmupSetsReps"));
		cntr.push($("#cmbWarmupIntensity"));
		if (!isModelStateValid(cntr))
			return;

		//#endregion

		var entry = getRow(warmupSlNo, warmups);
		entry.DayFrom = $("#txtWarmupDayFrom").val();
		if ($("#txtWarmupDayTo").val() == "")
			entry.DayTo = entry.DayFrom;
		else
			entry.DayTo = $("#txtWarmupDayTo").val();

		entry.Activity = $("#txtWarmupActivity").val();
		entry.TimeDist = $("#txtWarmupTimeDist").val();
		entry.SetsReps = $("#txtWarmupSetsReps").val();
		entry.IntensityId = $("#cmbWarmupIntensity").val();
		entry.Intensity = $("#cmbWarmupIntensity option:selected").text();
		entry.Notes = $("#txtWarmupNotes").val();

		if (warmupSlNo == 0) {
			warmups.push(entry);
		}
		clearWarmUp();
		listWarmUp();
	});

	clearWarmUp = function () {
		warmupSlNo = 0;
		$("#txtWarmupDayFrom").val('');
		$("#txtWarmupDayTo").val('');
		$("#txtWarmupActivity").val('');
		$("#txtWarmupTimeDist").val('');
		$("#txtWarmupSetsReps").val('');
		$("#txtWarmupNotes").val('');
		$("#WarmUpModel").modal('hide');
		$("#btnWarmupAdd").val('Add');
	}

	listWarmUp = function () {

		gridName = "tblWarmupGrid";

		$("#" + gridName + " tbody").empty();
		$.each(warmups, function () {
			var entry = this;

			var days = entry.DayFrom;
			if (entry.DayFrom != entry.DayTo)
				days += "-" + entry.DayTo;

			$("#" + gridName + " tbody")
				.append('<tr />')
				.children('tr:last')
				.append('<td style="width:10px;" class="grid-button" data-toggle="tooltip" data-placement="bottom" title="Edit" onclick= "editWarmupEntry(' + entry.SlNo + ');"><div class="glyphicon glyphicon-pencil"></div></td>')
				.append('<td style="width:10px;" class="grid-button" data-toggle="tooltip" data-placement="bottom" title="Delete" onclick= "deleteWarmupEntry(' + entry.SlNo + ');"><div class="glyphicon glyphicon-trash"></div></td>')
				.append('<td>' + days + '</td>')
				.append('<td>' + entry.Activity + '</td>')
				.append('<td>' + entry.TimeDist + '</td>')
				.append('<td>' + entry.SetsReps + '</td>')
				.append('<td>' + entry.Intensity + '</td>')
				.append('<td>' + entry.Notes + '</td>');
		});
	}

	editWarmupEntry = function (slno) {

		var entry = getRow(slno, warmups);
		warmupSlNo = slno;

		$("#txtWarmupDayFrom").val(entry.DayFrom);
		if (entry.DayFrom != entry.DayTo)
			$("#txtWarmupDayTo").val(entry.DayTo);
		else
			$("#txtWarmupDayTo").val('');
		$("#txtWarmupActivity").val(entry.Activity);
		$("#txtWarmupTimeDist").val(entry.TimeDist);
		$("#txtWarmupSetsReps").val(entry.SetsReps);
		$("#cmbWarmupIntensity").val(entry.IntensityId);
		$("#txtWarmupNotes").val(entry.Notes);
		$("#btnWarmupAdd").val('Update');
		$("#WarmUpModel").modal('show');
		$("#txtWarmupDays").focus();
	}

	deleteWarmupEntry = function (slno) {
		if (confirm('Are you sure want to remove?')) {
			deleteRow(slno, warmups);
			listWarmUp();
		}
	}

	//#endregion

	//#region Strength Section

	$("#btnStrengthAdd").click(function () {

		//#region validation

		cntr = [];
		cntr.push($("#txtStrengthDayFrom"));
		cntr.push($("#txtStrengthExcercise"));
		cntr.push($("#txtStrengthSetsReps"));
		cntr.push($("#txtStrengthWeight"));
		cntr.push($("#txtStrengthRestTime"));
		if (!isModelStateValid(cntr))
			return;

		//#endregion

		var entry = getRow(strengthSlNo, strengths);
		entry.DayFrom = $("#txtStrengthDayFrom").val();
		if ($("#txtStrengthDayTo").val() == "")
			entry.DayTo = entry.DayFrom;
		else
			entry.DayTo = $("#txtStrengthDayTo").val();
		entry.Excercise = $("#txtStrengthExcercise").val();
		entry.SetsReps = $("#txtStrengthSetsReps").val();
		entry.Weight = $("#txtStrengthWeight").val();
		entry.RestTime = $("#txtStrengthRestTime").val();
		entry.Notes = $("#txtStrengthNotes").val();

		if (strengthSlNo == 0) {
			strengths.push(entry);
		}
		clearStrengths();
		listStrength();
	});

	clearStrengths = function () {
		strengthSlNo = 0;
		$("#txtStrengthDayFrom").val('');
		$("#txtStrengthDayTo").val('');
		$("#txtStrengthExcercise").val('');
		$("#txtStrengthSetsReps").val('');
		$("#txtStrengthWeight").val('');
		$("#txtStrengthRestTime").val('');
		$("#txtStrengthNotes").val('');
		$("#StrengthModel").modal('hide');
		$("#btnStrengthAdd").val('Add');
	}

	listStrength = function () {

		gridName = "tblStrengthGrid";

		$("#" + gridName + " tbody").empty();
		$.each(strengths, function () {
			var entry = this;

			var days = entry.DayFrom;
			if (entry.DayFrom != entry.DayTo)
				days += "-" + entry.DayTo;

			$("#" + gridName + " tbody")
				.append('<tr />')
				.children('tr:last')
				.append('<td style="width:10px;" class="grid-button" data-toggle="tooltip" data-placement="bottom" title="Edit" onclick= "editStrengthEntry(' + entry.SlNo + ');"><div class="glyphicon glyphicon-pencil"></div></td>')
				.append('<td style="width:10px;" class="grid-button" data-toggle="tooltip" data-placement="bottom" title="Delete" onclick= "deleteStrengthEntry(' + entry.SlNo + ');"><div class="glyphicon glyphicon-trash"></div></td>')
				.append('<td>' + days+ '</td>')
				.append('<td>' + entry.Excercise + '</td>')
				.append('<td>' + entry.SetsReps + '</td>')
				.append('<td>' + entry.Weight + '</td>')
				.append('<td>' + entry.RestTime + '</td>')
				.append('<td>' + entry.Notes + '</td>');
		});
	}

	editStrengthEntry = function (slno) {

		var entry = getRow(slno, strengths);
		strengthSlNo = slno;

		$("#txtStrengthDayFrom").val(entry.DayFrom);
		if (entry.DayFrom != entry.DayTo)
			$("#txtStrengthDayTo").val(entry.DayTo);
		else
			$("#txtStrengthDayTo").val('');
		$("#txtStrengthExcercise").val(entry.Excercise);
		$("#txtStrengthSetsReps").val(entry.SetsReps);
		$("#txtStrengthWeight").val(entry.Weight);
		$("#txtStrengthRestTime").val(entry.RestTime);
		$("#txtStrengthNotes").val(entry.Notes);
		$("#btnStrengthAdd").val('Update');
		$("#StrengthModel").modal('show');
		$("#txtStrengthDays").focus();
	}

	deleteStrengthEntry = function (slno) {
		if (confirm('Are you sure want to remove?')) {
			deleteRow(slno, strengths);
			listStrength();
		}
	}

	//#endregion

	//#region Cardio Section

	$("#btnCardioAdd").click(function () {

		//#region validation

		cntr = [];
		cntr.push($("#txtCardioDayFrom"));
		cntr.push($("#txtCardioExcercise"));
		cntr.push($("#txtCardioTimeDist"));
		cntr.push($("#txtCardioTargetHR"));
		cntr.push($("#cmbCardioIntensity"));
		if (!isModelStateValid(cntr))
			return;

		//#endregion

		var entry = getRow(cardioSlNo, cardios);
		entry.DayFrom = $("#txtCardioDayFrom").val();
		if ($("#txtCardioDayTo").val() == "")
			entry.DayTo = entry.DayFrom;
		else
			entry.DayTo = $("#txtCardioDayTo").val();
		entry.Excercise = $("#txtCardioExcercise").val();
		entry.TimeDist = $("#txtCardioTimeDist").val();
		entry.TargetHR = $("#txtCardioTargetHR").val();
		entry.IntensityId = $("#cmbCardioIntensity").val();
		entry.Intensity = $("#cmbCardioIntensity option:selected").text();
		entry.Notes = $("#txtCardioNotes").val();

		if (cardioSlNo == 0) {
			cardios.push(entry);
		}
		clearcardios();
		listCardio();
	});

	clearcardios = function () {
		cardioSlNo = 0;
		$("#txtCardioDayFrom").val('');
		$("#txtCardioDayTo").val('');
		$("#txtCardioExcercise").val('');
		$("#txtCardioTimeDist").val('');
		$("#txtCardioTargetHR").val('');
		$("#txtCardioNotes").val('');
		$("#CardioModel").modal('hide');
		$("#btnCardioAdd").val('Add');
	}

	listCardio = function () {

		gridName = "tblCardioGrid";

		$("#" + gridName + " tbody").empty();
		$.each(cardios, function () {
			var entry = this;

			var days = entry.DayFrom;
			if (entry.DayFrom != entry.DayTo)
				days += "-" + entry.DayTo;


			$("#" + gridName + " tbody")
				.append('<tr />')
				.children('tr:last')
				.append('<td style="width:10px;" class="grid-button" data-toggle="tooltip" data-placement="bottom" title="Edit" onclick= "editCardioEntry(' + entry.SlNo + ');"><div class="glyphicon glyphicon-pencil"></div></td>')
				.append('<td style="width:10px;" class="grid-button" data-toggle="tooltip" data-placement="bottom" title="Delete" onclick= "deleteCardioEntry(' + entry.SlNo + ');"><div class="glyphicon glyphicon-trash"></div></td>')
				.append('<td>' + days + '</td>')
				.append('<td>' + entry.Excercise + '</td>')
				.append('<td>' + entry.TimeDist + '</td>')
				.append('<td>' + entry.TargetHR + '</td>')
				.append('<td>' + entry.Intensity + '</td>')
				.append('<td>' + entry.Notes + '</td>');
		});
	}

	editCardioEntry = function (slno) {

		var entry = getRow(slno, cardios);
		cardioSlNo = slno;

		$("#txtCardioDayFrom").val(entry.DayFrom);
		if (entry.DayFrom != entry.DayTo)
			$("#txtCardioDayTo").val(entry.DayTo);
		else
			$("#txtCardioDayTo").val('');
		$("#txtCardioExcercise").val(entry.Excercise);
		$("#txtCardioTimeDist").val(entry.TimeDist);
		$("#txtCardioTargetHR").val(entry.TargetHR);
		$("#cmbCardioIntensity").val(entry.IntensityId);
		$("#txtCardioNotes").val(entry.Notes);
		$("#btnCardioAdd").val('Update');
		$("#CardioModel").modal('show');
		$("#txtCardioDays").focus();
	}

	deleteCardioEntry = function (slno) {
		if (confirm('Are you sure want to remove?')) {
			deleteRow(slno, cardios);
			listCardio();
		}
	}

	//#endregion

	//#region Cooldown Section

	$("#btnCooldownAdd").click(function () {

		//#region validation

		cntr = [];
		cntr.push($("#txtCoolDownDayFrom"));
		cntr.push($("#txtCoolDownActivity"));
		cntr.push($("#txtCoolDownTimeDist"));
		cntr.push($("#txtCoolDownSetsReps"));
		cntr.push($("#cmbCoolDownIntensity"));
		if (!isModelStateValid(cntr))
			return;

		//#endregion

		var entry = getRow(cooldownSlNo, cooldowns);
		entry.DayFrom = $("#txtCoolDownDayFrom").val();
		if ($("#txtCoolDownDayTo").val() == "")
			entry.DayTo = entry.DayFrom;
		else
			entry.DayTo = $("#txtCoolDownDayTo").val();
		entry.Activity = $("#txtCoolDownActivity").val();
		entry.TimeDist = $("#txtCoolDownTimeDist").val();
		entry.SetsReps = $("#txtCoolDownSetsReps").val();
		entry.IntensityId = $("#cmbCoolDownIntensity").val();
		entry.Intensity = $("#cmbCoolDownIntensity option:selected").text();
		entry.Notes = $("#txtCoolDownNotes").val();

		if (cooldownSlNo == 0) {
			cooldowns.push(entry);
		}
		clearCooldown();
		listCooldown();
	});

	clearCooldown = function () {
		cooldownSlNo = 0;
		$("#txtCoolDownDayFrom").val('');
		$("#txtCoolDownDayTo").val('');
		$("#txtCoolDownActivity").val('');
		$("#txtCoolDownTimeDist").val('');
		$("#txtCoolDownSetsReps").val('');
		$("#txtCoolDownNotes").val('');
		$("#CoolDownModel").modal('hide');
		$("#btnCooldownAdd").val('Add');
	}

	listCooldown = function () {

		gridName = "tblCooldownGrid";

		$("#" + gridName + " tbody").empty();
		$.each(cooldowns, function () {
			var entry = this;

			var days = entry.DayFrom;
			if (entry.DayFrom != entry.DayTo)
				days += "-" + entry.DayTo;

			$("#" + gridName + " tbody")
				.append('<tr />')
				.children('tr:last')
				.append('<td style="width:10px;" class="grid-button" data-toggle="tooltip" data-placement="bottom" title="Edit" onclick= "editCooldownEntry(' + entry.SlNo + ');"><div class="glyphicon glyphicon-pencil"></div></td>')
				.append('<td style="width:10px;" class="grid-button" data-toggle="tooltip" data-placement="bottom" title="Delete" onclick= "deleteCooldownEntry(' + entry.SlNo + ');"><div class="glyphicon glyphicon-trash"></div></td>')
				.append('<td>' + days + '</td>')
				.append('<td>' + entry.Activity + '</td>')
				.append('<td>' + entry.TimeDist + '</td>')
				.append('<td>' + entry.SetsReps + '</td>')
				.append('<td>' + entry.Intensity + '</td>')
				.append('<td>' + entry.Notes + '</td>');
		});
	}

	editCooldownEntry = function (slno) {

		var entry = getRow(slno, cooldowns);
		cooldownSlNo = slno;

		$("#txtCoolDownDayFrom").val(entry.DayFrom);
		if (entry.DayFrom != entry.DayTo)
			$("#txtCoolDownDayTo").val(entry.DayTo);
		else
			$("#txtCoolDownDayTo").val('');
		$("#txtCoolDownActivity").val(entry.Activity);
		$("#txtCoolDownTimeDist").val(entry.TimeDist);
		$("#txtCoolDownSetsReps").val(entry.SetsReps);
		$("#cmbCoolDownIntensity").val(entry.IntensityId);
		$("#txtCoolDownNotes").val(entry.Notes);
		$("#btnCooldownAdd").val('Update');
		$("#CoolDownModel").modal('show');
		$("#txtCoolDownDays").focus();
	}

	deleteCooldownEntry = function (slno) {
		if (confirm('Are you sure want to remove?')) {
			deleteRow(slno, cooldowns);
			listCooldown();
		}
	}

	//#endregion

	//#region Save

	$("#btnWorkoutSave").click(function () {

		//#region validation

		cntr = [];
		cntr.push($("#txtMembershipNo"));
		cntr.push($("#txtDateFrom"));
		cntr.push($("#txtDateTo"));
		if (!isModelStateValid(cntr))
			return;

		//#endregion

		var obj = {
			Id: masterId,
			CustomerId: customerId,
			FromDate: $("#txtDateFrom").val(),
			ToDate: $("#txtDateTo").val(),
			Goal: $("#txtGoal").val(),
			Warmups: warmups,
			Strengths: strengths,
			Cardios: cardios,
			Cooldowns: cooldowns
		};
		Post("workout/save", obj, "saved");
	});

	saved = function (data) {
		if (data.Response.IsError) {
			errorMessage(data.Response.Response);
			return;
		}
		else {
			location.href = `/WorkoutPlan/WorkoutChart?id=${data.Response.Response}`;
		}
		//successMessage("Saved Successfully");
		//if (masterId != "") {
		//	location.href = '/Customer?id=' + customerId;
		//}
		//clear();
		$("#txtMembershipNo").focus();
	}

	clear = function () {
		masterId = "";
		$("#txtMembershipNo").val('');
		$("#txtFirstName").val('');
		$("#txtDateFrom").val('');
		$("#txtDateTo").val('');
		$("#txtGoal").val('');
		warmups = [];
		strengths = [];
		cardios = [];
		cooldowns = [];
		clearWarmUp();
		listWarmUp();
		clearStrengths();
		listStrength();
		clearcardios();
		listCardio();
		clearCooldown();
		listCooldown();
	}

	//#endregion

	//#region Master
	fillMasterData = function (data) {
		masterId = data.Id;
		customerId = data.CustomerId;
		$("#txtDateFrom").val(new Date(data.FromDate).toDateInputValue());
		$("#txtDateTo").val(new Date(data.ToDate).toDateInputValue());
		$("#txtGoal").val(data.Goal);
		$("#txtMembershipNo").val(data.MembershipNo);
		$("#txtFirstName").val(data.Name);
	}
	//#endregion

	//#region Save

	$("#btnNew").click(function () {
		location.href = '/WorkoutPlan';
	});
	
	//#endregion

});